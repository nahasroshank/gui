﻿/* Nahas Roshan Kunnathodika
 * facebook.com/nahasroshank
 * nahasroshank@gmail.com */

using System;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Threading;


// program to interact with NGSPICE library ( send data and collect results )

namespace ngshared_test
{
    // interface for DllLoadUtilsWindows class
    interface DllLoadUtils
    {
        IntPtr LoadLibrary(String fileName);
        void FreeLibrary(IntPtr handle);
        IntPtr GetProcAddress(IntPtr dllHandle, String name);
    }
    struct variableInfo
    {
        public int index;
        public string label;
        public string unit;
    }

    // class with Dll loading and related functions
    public class DllLoadUtilsWindows : DllLoadUtils
    {
        void DllLoadUtils.FreeLibrary(IntPtr handle)
        {
            FreeLibrary(handle);
        }

        IntPtr DllLoadUtils.GetProcAddress(IntPtr dllHandle, String name)
        {
            return GetProcAddress(dllHandle, name);
        }

        IntPtr DllLoadUtils.LoadLibrary(String fileName)
        {
            return LoadLibrary(fileName);
        }

        // Importing useful functions from kernel32.dll
        [DllImport("kernel32")]
        public static extern IntPtr LoadLibrary(String fileName);

        [DllImport("kernel32.dll")]
        public static extern int FreeLibrary(IntPtr handle);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetProcAddress(IntPtr handle, String procedureName);
    }
   
    // Contains code for creating standalone console version of NGSPICE simulator.
    // All console.read() and console.write() functions work only when this project is compiled
    // as a seperate executable, with function MainSpice() renamed to Main()
    public class ProgramSpice
    {
        public static vecvaluesall vectorAllObject;
        public static vecvalues[] vectorArray;
        public static void Message()
        {
            Console.WriteLine("NGSPICE CONSOLE");
            Console.WriteLine("**************************************************");
        }
        
        // function to load NGSPICE library from dll file
        public static IntPtr LoadNGSpiceLibrary([MarshalAs(UnmanagedType.LPStr)]String loadstring)
        {
            error_ngspice = false;
            Console.WriteLine("Loading ngspice.dll...");
            IntPtr ngspiceDllHandle = DllLoadUtilsWindows.LoadLibrary(loadstring);
            if (ngspiceDllHandle == IntPtr.Zero)
            {
                error_ngspice = true;
                Console.WriteLine("Could not load ngspice.dll");
                Console.WriteLine("Exiting...");
                Console.ReadKey();
                return ngspiceDllHandle;
                //Controlled exit - here?
            }
            else
            {
                Console.WriteLine("ngspice.dll loaded");
                int freeLibraryStatus = DllLoadUtilsWindows.FreeLibrary(ngspiceDllHandle);
                return ngspiceDllHandle;
            }
        }
        public static bool error_ngspice, NoBGProcessRunning = true;

        // Main function. Change name to Main() and execute along with related .cs files to create standalone console
        // version 
        public static void MainSpice(String[] args)
        {
            String loadstring = "ngspice.dll";
            String consoleInput, currentPlot;
            IntPtr allVectorNamePointer = IntPtr.Zero;
            String[] splitConsoleInput;
            IntPtr ngspiceDllHandle = IntPtr.Zero;
            Message();
            ngspiceDllHandle = LoadNGSpiceLibrary(loadstring);
            if (error_ngspice)
            {
                Console.ReadKey();
                return;
            }
            
            // functions to send and receive data from library
            SendChar sc = new SendChar(ng_getchar);
            SendStat ss = new SendStat(ng_getstat);
            ControlledExit ce = new ControlledExit(ng_exit);
            SendData sd = new SendData(ng_data);
            SendInitData sid = new SendInitData(ng_initData);
            BGThreadRunning bgtrun = new BGThreadRunning(ng_thread_runs);
            int thread = Thread.CurrentThread.ManagedThreadId;
            IntPtr caller = Marshal.AllocHGlobal(Marshal.SizeOf(thread));
            Marshal.StructureToPtr(thread, caller, false);
            NativeMethods.ngSpice_Init(
                Marshal.GetFunctionPointerForDelegate(sc),
                Marshal.GetFunctionPointerForDelegate(ss),
                Marshal.GetFunctionPointerForDelegate(ce),
                Marshal.GetFunctionPointerForDelegate(sd),
                Marshal.GetFunctionPointerForDelegate(sid),
                Marshal.GetFunctionPointerForDelegate(bgtrun),
                caller);
            do
            {
                if (args.Length > 0)
                {

                    for (int i = 0; i < args.Length; i++)
                    {
                        if (args[i].Split('.')[1] == "cir")
                        {
                            
                            Console.WriteLine("source " + args[i]);
                            NativeMethods.ngSpice_Command("source " + args[i]);
                            Console.WriteLine("run");
                            NativeMethods.ngSpice_Command("run");
                            Console.WriteLine("write " + args[i].Split('.')[0] + ".raw");
                            NativeMethods.ngSpice_Command("write " + args[i].Split('.')[0] + ".raw");
                            //Console.WriteLine("wrs2p " + args[i].Split('.')[0] + ".raw");
                            S2PClass.rawTos2p(args[i].Split('.')[0] + ".raw");
                        }
                        if (args[i].Split('.')[1] == "raw")
                        {
                            Console.WriteLine("wrs2p " + args[i]);
                            S2PClass.rawTos2p(args[i]);
                        }
                    }
                    Console.WriteLine("S2P file conversion completed");
                    Console.WriteLine("Press enter to continue...");
                    MessageBox.Show("S2P file created","Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Console.ReadLine();
                    return;
                }
                error_ngspice = false;
                do
                {
                    Task.Delay(500);
                    if (NoBGProcessRunning == true)
                    {
                        Console.Write("> ");
                        break;
                    }
                    else Task.Delay(500);
                } while (true);
                consoleInput = Console.ReadLine();
                splitConsoleInput = consoleInput.Split(' ');
                switch (splitConsoleInput[0])
                {
                    case "wrs2p":
                        int s2p = S2PClass.rawTos2p(splitConsoleInput[1]);
                        if (s2p == 0) Console.WriteLine("S2P file conversion completed");
                        break;
                    case "exit":
                        NativeMethods.ngSpice_Command("quit");
                        break;
                    case "run":
                        NativeMethods.ngSpice_Command("run");
                        break;
                    case "bg_run":
                        NativeMethods.ngSpice_Command("bg_run");
                        break;
                    case "source":
                        error_ngspice = false;
                        int status = NativeMethods.ngSpice_Command(consoleInput);
                        for (int k2 = 0; k2 < 10; k2++)
                        {
                            Task.Delay(500); // to catch error
                            if (NoBGProcessRunning) break;
                        }
                        // to let the source file get processed and receive error details
                        //return output from library
                        break;
                    case "display":
                        currentPlot = Marshal.PtrToStringAnsi(NativeMethods.ngSpice_CurPlot());
                        Console.WriteLine("Current Plot: {0}", currentPlot);
                        for (int i = 0; i < vectorArray.Length; i++)
                        {
                            Console.WriteLine("VectorName {0}: {1}", i, vectorArray[i]);
                        }
                        break;
                    case "write":
                        String outputFilename = splitConsoleInput[1];
                        if (String.IsNullOrEmpty(outputFilename))
                        {
                            break;
                        }
                        if (splitConsoleInput.Length > 2)
                        {
                            //code to write specific output vectors to .raw file
                        }
                        while (true)
                        {
                            Task.Delay(500);
                            if (NoBGProcessRunning) break; //wait for background processes to finish
                        }
                        NativeMethods.ngSpice_Command(consoleInput);
                        break;

                    case "circbyline":
                        NativeMethods.ngSpice_Command(consoleInput);
                        if (splitConsoleInput[1] == ".end") NativeMethods.ngSpice_Command("bg_run");
                        break;
                    default:
                        break;

                }
            } while (true);
        }

        // definitions for NGSPICE library callback functions
        private static int ng_getchar(IntPtr outputReturn, int ident, IntPtr userData)
        {
            //Console.WriteLine("ng_getchar() running...");
            String message = Marshal.PtrToStringAnsi(outputReturn);
            Console.WriteLine(message);
            /*if (ciprefix("stderr", message))
            {
                Console.WriteLine(message);
                error_ngspice = true;
            }*/

            return 0;
        }
        private static int ng_getstat(IntPtr outputReturn, int indent, IntPtr userData)
        {
            //Console.WriteLine("ng_getstat() running...");
            String statusMessage = Marshal.PtrToStringAnsi(outputReturn);
            Console.WriteLine(statusMessage);
            return 0;
        }
        private static int ng_exit(int exitStatus, [MarshalAs(UnmanagedType.I1)] bool immediate, [MarshalAs(UnmanagedType.I1)] bool quitExit, int indent, IntPtr userData)
        {
            Console.WriteLine("Controlled exit...");
            Console.ReadKey();
            return exitStatus;
        }
        private static int ng_data(IntPtr vectorData, int vectorNumber, int indent, IntPtr userData) // output from ngspice
        {
            //Console.WriteLine("ng_data() running...");
            unsafe
            {
                vectorAllObject = (vecvaluesall)Marshal.PtrToStructure(vectorData, typeof(vecvaluesall));
                IntPtr vectorArrayPointer = vectorAllObject.vecsa;
                vectorArray = new vecvalues[vectorAllObject.veccount];
                for (int i = 0; i < vectorAllObject.veccount; i++)
                {
                    vectorArray[i] = (vecvalues)Marshal.PtrToStructure(vectorArrayPointer + i * sizeof(IntPtr), typeof(vecvalues));
                    //Console.WriteLine("{0}", vectorArray[i].creal);
                }
            }
            return 1;
        }
        private static int ng_initData(IntPtr intData, int ident, IntPtr userData) // data before simulation
        {
            Console.WriteLine("ng_initData() running...");
            
            {

                vecinfoall vectorInfoAllObject;
                vectorInfoAllObject = (vecinfoall)Marshal.PtrToStructure(intData, typeof(vecinfoall));
                int vectorCount = vectorInfoAllObject.veccount;
                IntPtr vectorInfoArrayPointer = vectorInfoAllObject.vecs;
                /*vecinfo[] vectorInfoArray = StructArrayFromIntPtr<vecinfo>(vectorInfoArrayPointer, vectorCount);
                for (int i = 0; i < vectorCount; i++)
                {
                    String name = vectorInfoArray[i].vecname;
                    Console.WriteLine("Vector {0}: {1}", i, name);
                }*/
                /*
                int totalSize = sizeof(int) + sizeof(char*) + sizeof(bool) + sizeof(void*) + sizeof(void*);
                Console.WriteLine("Number of vectors: {0}", vectorCount);
                
                Marshal.StringToCoTaskMemUni()
                vecinfo* vectorInfoStructPointer = (vecinfo*)vectorInfoArrayPointer.ToPointer(); 
                String name = tempVector.vecname.ToString();
                Console.WriteLine(name);*/



                // code remaining

            }
            return 1;
        }
        private static int ng_thread_runs([MarshalAs(UnmanagedType.I1)] bool noRunStatus, int ident, IntPtr userData)
        {
            Console.WriteLine("ng_threadruns() running...");
            NoBGProcessRunning = noRunStatus;
            if (noRunStatus) Console.WriteLine("No Background Process Running");
            else Console.WriteLine("Background Process Running");
            return 1;
        }
        private static bool ciprefix(String refString, String testString)
        {
            int index = 0;
            while (index < refString.Length)
            {
                char pc = Char.ToLower(refString[index]);
                char sc = Char.ToLower(testString[index]);
                if (pc != sc) return (false);
                index++;
            }
            return true;
        }
        public static T[] StructArrayFromIntPtr<T>(IntPtr outArray, int size) where T : new()
        {
            T[] resArray = new T[size];
            IntPtr current = outArray;
            for (int i = 0; i < size; i++)
            {
                resArray[i] = new T();
                Marshal.PtrToStructure(current, resArray[i]);
                Marshal.DestroyStructure(current, typeof(T));
                int structsize = Marshal.SizeOf(resArray[i]);
                current = (IntPtr)((long)current + structsize);
            }
            Marshal.FreeCoTaskMem(outArray);
            return resArray;
        }
        public static int convertFromRawToS2P(String fileName)
        {
            String[] splitFileName = fileName.Split('.');
            String outputFileName = splitFileName[0] + ".s2p";
            FileStream inputFileStream = File.Open(fileName, FileMode.Open, FileAccess.Read);
            FileStream outputFileStream = File.Open(outputFileName, FileMode.Create);

            return 1;
        }
    }
}
