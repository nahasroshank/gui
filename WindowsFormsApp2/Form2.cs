﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        public Form2(int num)
        {
            Graphics graphics = this.CreateGraphics();
            InitializeComponent();
            if(num == 1)
            {
                this.Text = "Help";
                this.Name = "Help";
                richTextBox1.Text = "Value input format:\n\nKilo - K\nMega - Meg\nTera - T\nMilli - m\nMicro - u\nNano - n\nPico - p";
            }
            if(num == 2)
            {
                this.Text = "About";
                this.Name = "About";
                richTextBox1.Text = "Application to create S2P file from .RAW files or circuit schematic. The simulation is carried" +
                    " out using SPICE library\nDeveloper: Nahas Roshan Kunnathodika\nfacebook.com/nahasroshank\ne-mail: nahasroshank@gmail.com";
            }
        }
        public Form2(string fileName)
        {
            Graphics graphics = this.CreateGraphics();
            InitializeComponent();
            try
            {
                richTextBox1.LoadFile(fileName, RichTextBoxStreamType.PlainText);
                this.Text = fileName;
            }
            catch(Exception e) {
                if (e.GetType() == typeof(IOException))
                    MessageBox.Show("Error while opening file!", "IO Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if(e.GetType() == typeof(ArgumentException))
                    MessageBox.Show("Bad filename!", "File Name Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
