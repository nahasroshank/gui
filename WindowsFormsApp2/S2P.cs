﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
//extern struct variableInfo;
namespace ngshared_test
{
    class S2PClass
    {
        public static int[] GetRequiredIndex(variableInfo[] infoArray, string[] stringArray)
        {
            int[] returnArray = new int[stringArray.Length];
            int returnIndex = 0;
            for (int i = 0; i < infoArray.Length; i++)
            {
                for (int j = 0; j < stringArray.Length; j++)
                {
                    if (infoArray[i].label == stringArray[j])
                    {
                        returnArray[returnIndex] = i;
                        returnIndex++;
                    }
                }

            }
            return returnArray;
        }
        public static string GetHashString(StreamWriter handle, variableInfo[] infoArray, int[] indexArray)
        {
            string returnString = "# Hz S RI R 50";
            return returnString;

        }
        public static string PrintLabel()
        {
            string returnString = "!freq\tReS11\tImS11\tReS21\tImS22\tReS12\tImS12\tReS22\tImS22";
            return returnString;
        }
        public static int rawTos2p(string rawFilename)
        {
            string[] requiredVariables = { "frequency", "s11", "s21", "s12", "s22" };
            string s2pFilename = rawFilename.Split('.')[0] + ".s2p";
            System.IO.StreamReader rawFileHandle;
            try { rawFileHandle = new System.IO.StreamReader(rawFilename); }
            catch (Exception)
            {
                Console.WriteLine("Error opening {0}", rawFilename);
                return -1;
            }
            //rawFileHandle = new System.IO.StreamReader(rawFilename);
            
            System.IO.StreamWriter s2pFileHandle = new System.IO.StreamWriter(s2pFilename);
            int[] requiredIndex = new int[requiredVariables.Length];

            if (rawFilename == null)
            {
                Console.WriteLine("Error reading {0}", rawFilename);
                return -1;
            }
            string[] commentPrefix = { "Title:", "Date:", "Plotname:" };
            // # [Unit x-axis] [S/Y/Z Parameter] [Real-Imaginary] [Resistance] [ImpedanceValue]

            bool variableFlag = false, commentWordFlag = false, valuesFlag = false, newInstanceFlag = false;
            bool noOfVariablesDefined = false, noOfPointsDefined = false, writeFlag = false, searchCompletedFlag = false;
            string stringBuffer, writeBuffer = null;
            double noOfPoints, valueIndex = 0, real = 0, imag = 0;
            int variableIndex = 0, noOfVariables = 1;
            List<variableInfo> variableInfoArray = new List<variableInfo>();

            while ((stringBuffer = rawFileHandle.ReadLine()) != null)
            {
                string[] stringBufferSplit = stringBuffer.Split(' ', '\t', '\n', ',');
                List<string> stringBufferSplitList = new List<string>(stringBufferSplit);
                for (int v = 0; v < stringBufferSplitList.Count; v++)
                {
                    stringBufferSplitList.Remove("");
                }
                if (stringBufferSplitList.Count > 0)
                    switch (stringBufferSplitList[0])
                    {

                        case "Title:":
                        case "Date:":
                        case "Plotname:":
                            stringBuffer = "!" + stringBuffer;
                            s2pFileHandle.WriteLine(stringBuffer);
                            break;
                        case "Variables:":
                            variableFlag = true;
                            valuesFlag = false;
                            newInstanceFlag = true;

                            break;
                        case "Values:":

                            variableInfo[] variableInfoArray2 = variableInfoArray.ToArray();
                            for (int ii = 0; ii < variableInfoArray2.Length; ii++)
                            {
                                Console.WriteLine("{0} {1} {2}", variableInfoArray2[ii].index, variableInfoArray2[ii].label, variableInfoArray2[ii].unit);
                            }
                            requiredIndex = GetRequiredIndex(variableInfoArray2, requiredVariables);
                            Console.WriteLine("Required variables:");
                            for (int i1 = 0; i1 < requiredIndex.Length; i1++)
                            {
                                Console.WriteLine("{0}.{1}", requiredIndex[i1], variableInfoArray2[requiredIndex[i1]].label);

                            }
                            //Console.ReadKey();
                            string PrintHash = GetHashString(s2pFileHandle, variableInfoArray2, requiredIndex);
                            s2pFileHandle.WriteLine(PrintHash);
                            s2pFileHandle.WriteLine(PrintLabel());
                            valuesFlag = true;
                            variableFlag = false;
                            newInstanceFlag = true;
                            break;
                        case "No.:":
                            if (stringBufferSplitList[1] == "Variables:")
                            {
                                noOfVariables = Convert.ToInt32(stringBufferSplitList[2]);
                                noOfVariablesDefined = true;
                            }
                            if (stringBufferSplitList[1] == "Points:")
                            {
                                noOfPoints = Convert.ToInt32(stringBufferSplitList[2]);
                                noOfPointsDefined = true;
                            }
                            break;
                    }
                if (variableFlag && !newInstanceFlag)
                {
                    variableInfo tempObj = new variableInfo();
                    //Console.WriteLine(">>{0}",(int)stringBufferSplitList[0][0]);
                    tempObj.index = Convert.ToInt32(stringBufferSplitList[0]);
                    tempObj.label = stringBufferSplitList[1];
                    tempObj.unit = stringBufferSplitList[2];
                    variableInfoArray.Add(tempObj);

                }
                else if (valuesFlag && !newInstanceFlag)
                {
                    if (stringBufferSplitList.Count == 3)
                    {
                        searchCompletedFlag = true;
                        if (writeFlag && searchCompletedFlag)
                        {
                            s2pFileHandle.WriteLine(writeBuffer);
                            writeFlag = false;
                            searchCompletedFlag = false;
                        }
                        writeBuffer = null;
                        variableIndex = 0;
                        valueIndex = Double.Parse(stringBufferSplitList[0]);
                        real = Convert.ToDouble(stringBufferSplitList[1]);
                        imag = Convert.ToDouble(stringBufferSplitList[2]);
                    }
                    else if (stringBufferSplitList.Count == 2)
                    {
                        variableIndex++;
                        real = Convert.ToDouble(stringBufferSplitList[0]);
                        imag = Convert.ToDouble(stringBufferSplitList[1]);
                    }
                    for (int k = 0; k < requiredIndex.Length; k++)
                    {
                        int insertPosition = 0;
                        if (writeBuffer != null) insertPosition = writeBuffer.Length - 1;
                        if (insertPosition < 0 || writeBuffer == null) insertPosition = 0;
                        if (variableIndex == requiredIndex[k])
                        {
                            writeFlag = true;
                            writeBuffer = writeBuffer + real.ToString() + "\t";
                            if (variableInfoArray[variableIndex].label != "frequency")
                                writeBuffer = writeBuffer + imag.ToString() + "\t";
                        }
                    }

                }
                else if (newInstanceFlag)
                {
                    newInstanceFlag = false;
                }
            }
            s2pFileHandle.WriteLine(writeBuffer);
            s2pFileHandle.Close();
            rawFileHandle.Close();
            return 0;
        }
    }
}