﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using ngshared_test;

namespace WindowsFormsApp2
{

    public partial class Form1 : Form
    {
        public string currentS2PFile = "newNetList.s2p";
        public static int UNDEFINED = -1;
        public bool alignment = true;
        public static int LIN = 1, RES = 2, CAP = 3, IND = 4, GND = 5, INP = 6, OUT = 7;
        public static int LINEAR = 0, DECADE = 1, OCTAVE = 2;
        public static int BREAK = 0, DO_NOT_BREAK = 1;
        private Point point = new Point(20, 30), endPoint = new Point(40, 60);
        private bool startDone = false, continueClick = false, firstClick = true;

        public int nodeIndex = 1;

        public bool toggleShape = false;
        SParameterData sParameterData = new SParameterData();
        
        public int component = 0;
        static public bool componentSelected = false;
        public int selectedType, selectedIndex;
        static public int componentDimension = 40;
        static public int gridDimension = 10;
        List<int> nodeList = new List<int>();
        List<object> nextComponent = new List<object>();

        Rectangle gridSnapRectangle;
        Rectangle greyRectangle;

        public GreyList greyList = new GreyList();
        public ComponentList newList = new ComponentList();
        public ComponentList pendingList = new ComponentList();

        

        public Form1()
        {
            Graphics graphics = this.CreateGraphics();
           
            
            InitializeComponent();
            InitializeComponentUserDefined();

        }
        protected void InitializeComponentUserDefined()
        {
            //DrawGridPoints();
            Color oldColor = this.BackColor;
            this.BackColor = Color.FromArgb(245, 245, 200);
            this.Text = "S-Parameters for Single Port Networks";
            this.Name = "S_PARAM";
            
        }
        public Point gridCoordinates(Point point)
        {
            Point gridPoint = new Point();
            Point halfPoint = new Point(gridDimension / 2, gridDimension / 2);
            int remainder;
            gridPoint.X = gridDimension * Math.DivRem(point.X + halfPoint.X, gridDimension, out remainder);
            gridPoint.Y = gridDimension * Math.DivRem(point.Y + halfPoint.Y, gridDimension, out remainder);
            return gridPoint;
        }
        protected void SelectComponent(int type, int index)
        {
            selectedType = type;
            selectedIndex = index;
            if (type == LIN)
                greyRectangle = newList.lineList[index].imageRectangle;
            if (type == RES)
                greyRectangle = newList.resistorList[index].imageRectangle;
            if (type == IND)
                greyRectangle = newList.inductorList[index].imageRectangle;
            if (type == CAP)
                greyRectangle = newList.capacitorList[index].imageRectangle;
            if (type == GND)
                greyRectangle = newList.groundList[index].imageRectangle;
            if (type == INP)
                greyRectangle = newList.inputPortList[index].imageRectangle;
            if (type == OUT)
                greyRectangle = newList.outputPortList[index].imageRectangle;


        }
        public void CheckClickableZone()
        {
            componentSelected = false;
            greyRectangle = Rectangle.Empty;
            selectedIndex = -1;
            selectedType = -1;
            for (int i = 0; i < newList.resistorList.Count; i++)
            {
                if (newList.resistorList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(RES, i);
                    componentSelected = true;
                    break;
                }
            }
            for (int i = 0; i < newList.inductorList.Count; i++)
            {
                if (newList.inductorList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(IND, i);
                    componentSelected = true;
                    break;
                }
            }
            for (int i = 0; i < newList.capacitorList.Count; i++)
            {
                if (newList.capacitorList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(CAP, i);
                    componentSelected = true;
                    break;
                }
            }
            for (int i = 0; i < newList.groundList.Count; i++)
            {
                if (newList.groundList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(GND, i);
                    componentSelected = true;
                    break;
                }
            }
            for (int i = 0; i < newList.lineList.Count; i++)
            {
                if (newList.lineList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(LIN, i);
                    componentSelected = true;
                    break;
                }
            }
            for (int i = 0; i < newList.inputPortList.Count; i++)
            {
                if (newList.inputPortList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(INP, i);
                    componentSelected = true;
                    break;
                }
            }
            for (int i = 0; i < newList.outputPortList.Count; i++)
            {
                if (newList.outputPortList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(OUT, i);
                    componentSelected = true;
                    break;
                }
            }
            toolStripLabel3.Enabled = componentSelected;
            this.toolStripLabel5.Enabled = componentSelected;
            UpdateToolStripTextBox1TextShowDetails(false);
            toolStrip1.Refresh();
       
        }
        protected void UpdateToolStripTextBox1TextShowDetails(bool click)
        {
            if(selectedType == RES)
            {
                if (newList.resistorList[selectedIndex].showDetails == true)
                {
                    this.toolStripLabel5.Text = "Hide Details";
                    if(click == true) newList.resistorList[selectedIndex].showDetails = false;
                }
                else
                {
                    this.toolStripLabel5.Text = "Show details";
                    if (click == true) newList.resistorList[selectedIndex].showDetails = true;
                }


            }
            if (selectedType == CAP)
            {
                if (newList.capacitorList[selectedIndex].showDetails == true)
                {
                    this.toolStripLabel5.Text = "Hide Details";
                    if (click == true) newList.capacitorList[selectedIndex].showDetails = false;
                }
                else
                {
                    this.toolStripLabel5.Text = "Show details";
                    if (click == true) newList.capacitorList[selectedIndex].showDetails = true;
                }
            }
            if (selectedType == IND)
            {
                if (newList.inductorList[selectedIndex].showDetails == true)
                {
                    this.toolStripLabel5.Text = "Hide Details";
                    if (click == true) newList.inductorList[selectedIndex].showDetails = false;
                }
                else
                {
                    this.toolStripLabel5.Text = "Show details";
                    if (click == true) newList.inductorList[selectedIndex].showDetails = true;
                }
            }
            toolStrip1.Refresh();
        }
        protected void EditComponent(int type, int index)
        {
            string defaultValue = null;
            string valueString = null;
            double editValue = 0;
            if (type == RES || type == CAP || type == IND)
            {
                if (type == RES)
                {
                    valueString = "Resistance";
                    defaultValue = newList.resistorList[index].value.ToString();
                }
                if (type == IND)
                {
                    valueString = "Inductance";
                    defaultValue = newList.inductorList[index].value.ToString();
                }
                if (type == CAP)
                {
                    valueString = "Capacitance";
                    defaultValue = newList.capacitorList[index].value.ToString();
                }
                string Message = "Enter value of " + valueString + ":";
                string input = Interaction.InputBox(Message, "Edit component: ", ManagedUnit(defaultValue));
                input = UnmanagedUnit(input);
                bool goodValue = double.TryParse(input, out editValue);
                if (type == RES && goodValue)
                {
                    newList.resistorList[index].value = editValue;
                }
                if (type == IND && goodValue)
                {
                    newList.inductorList[index].value = editValue;
                }
                if (type == CAP && goodValue)
                {
                    newList.capacitorList[index].value = editValue;
                }
            }
        }
        public void CheckDoubleClickableZone()

        {
            for (int i = 0; i < newList.resistorList.Count; i++)
            {
                if (newList.resistorList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(RES, i);
                    EditComponent(RES, i);
                    break;
                }
            }
            for (int i = 0; i < newList.inductorList.Count; i++)
            {
                if (newList.inductorList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(IND, i);
                    EditComponent(IND, i);
                    break;
                }
            }
            for (int i = 0; i < newList.capacitorList.Count; i++)
            {
                if (newList.capacitorList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(CAP, i);
                    EditComponent(CAP, i);
                    break;
                }
            }
            for (int i = 0; i < newList.groundList.Count; i++)
            {
                if (newList.groundList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(GND, i);
                    EditComponent(GND, i);
                    break;
                }
            }
            for (int i = 0; i < newList.lineList.Count; i++)
            {
                if (newList.lineList[i].imageRectangle.Contains(this.PointToClient(Control.MousePosition)))
                {
                    SelectComponent(LIN, i);
                    break;
                }
            }
        }
        protected void DrawGridSnap()
        {
            this.CreateGraphics().DrawRectangle(new Pen(Color.Red), gridSnapRectangle);
        }
        /*protected void DrawGridPoints()
        {
            for (int i = gridDimension; i < this.Size.Width; i = i + gridDimension)
                for (int j = gridDimension; j < this.Size.Height; j = j + gridDimension)
                {
                    this.CreateGraphics().DrawRectangle(new Pen(Color.Red), new Rectangle(i - 1, j - 1, 2, 2));
                }
        }*/
        /*protected void SearchComponent(Point terminal)
        {

            ElectricalComponent returnComponent = null;
            Line returnLine = null;
            List<Line> returnLineList = new List<Line>();
            List<ElectricalComponent> returnComponentList = new List<ElectricalComponent>();
            for (int i = 0; i < pendingList.lineList.Count; i++)
            {
                if (pendingList.lineList[i].terminal1 == terminal || pendingList.lineList[i].terminal2 == terminal)
                {
                    returnLine = pendingList.lineList[i];
                    returnLineList.Add(returnLine);
                    pendingList.lineList.RemoveAt(i);
                }
            }
            for (int i = 0; i < pendingList.resistorList.Count; i++)
            {
                if (pendingList.resistorList[i].terminal1 == terminal || pendingList.resistorList[i].terminal2 == terminal)
                {
                    returnComponent = pendingList.resistorList[i];
                    returnComponentList.Add(returnComponent);
                    pendingList.resistorList.RemoveAt(i);
                }
            }
            for (int i = 0; i < pendingList.capacitorList.Count; i++)
            {
                if (pendingList.capacitorList[i].terminal1 == terminal || pendingList.capacitorList[i].terminal2 == terminal)
                {
                    returnComponent = pendingList.capacitorList[i];
                    returnComponentList.Add(returnComponent);
                    pendingList.capacitorList.RemoveAt(i);
                }
            }
            for (int i = 0; i < pendingList.inductorList.Count; i++)
            {
                if (pendingList.inductorList[i].terminal1 == terminal || pendingList.inductorList[i].terminal2 == terminal && pendingList.lineList[i].check == false)
                {
                    returnComponent = pendingList.inductorList[i];
                    returnComponentList.Add(returnComponent);
                    pendingList.inductorList.RemoveAt(i);
                }
            }

            /*for (int i = 0; i < returnComponentList.Count; i++)
            {
                returnComponentList[i].node1 = GetExistingNode(terminal);
                if (GetExistingNode(returnComponentList[i].terminal2) != -1) returnComponentList[i].node2 = GetExistingNode(returnComponentList[i].terminal2);
                else
                {
                    nodeList.Add(nodeList.Last() + 1);
                    returnComponentList[i].node2 = nodeList.Last();
                }
                returnComponent = returnComponentList[i];
                CopyDetailsToList(returnComponent);
                
            }
            for (int i = 0; i < returnComponentList.Count; i++)
            {
                SearchComponent(returnComponentList[i].terminal1);
                SearchComponent(returnComponentList[i].terminal2);
            }
        }*/
        internal void CopyDetailsToList(object arg)
        {
            if (arg.GetType() == typeof(Resistor))
            {
                Resistor argComponent = (Resistor)arg;
                for (int j = 0; j < newList.resistorList.Count; j++)
                    if (argComponent.terminal1 == newList.resistorList[j].terminal1 && argComponent.terminal2 == newList.resistorList[j].terminal2)
                    {
                        newList.resistorList[j].node1 = argComponent.node1;
                        newList.resistorList[j].node2 = argComponent.node2;
                    }
            }
            else if (arg.GetType() == typeof(Capacitor))
            {
                Capacitor argComponent = (Capacitor)arg;
                for (int j = 0; j < newList.capacitorList.Count; j++)
                    if (argComponent.terminal1 == newList.capacitorList[j].terminal1 && argComponent.terminal2 == newList.capacitorList[j].terminal2)
                    {
                        newList.capacitorList[j].node1 = argComponent.node1;
                        newList.capacitorList[j].node2 = argComponent.node2;
                    }
            }
            else if (arg.GetType() == typeof(Inductor))
                for (int j = 0; j < newList.inductorList.Count; j++)
                {
                    Inductor argComponent = (Inductor)arg;
                    if (argComponent.terminal1 == newList.inductorList[j].terminal1 && argComponent.terminal2 == newList.inductorList[j].terminal2)
                    {
                        newList.inductorList[j].node1 = argComponent.node1;
                        newList.inductorList[j].node2 = argComponent.node2;
                    }
                }
            else if (arg.GetType() == typeof(Line))
            {
                Line argComponent = (Line)arg;
                for (int j = 0; j < newList.lineList.Count; j++)
                    if (argComponent.terminal1 == newList.lineList[j].terminal1 && argComponent.terminal2 == newList.lineList[j].terminal2)
                    {
                        newList.lineList[j].node1 = argComponent.node1;
                        newList.lineList[j].node2 = argComponent.node2;
                    }
            }
        }
        protected bool[] CheckGenerateNetlistConditions()
        {
            bool[] check = new bool[] { true, true };
            if (newList.inputPortList.Count != 1 || newList.outputPortList.Count != 1)
                check[0] = false;
            for (int i = 0; i < newList.inputPortList.Count; i++)
            {
                if (newList.inputPortList[i].node == 0)
                {
                    check[1] = false;
                    break;
                }
            }
            for (int i = 0; i < newList.outputPortList.Count; i++)
            {
                if (newList.outputPortList[i].node == 0)
                {
                    check[1] = false;
                    break;
                }
            }
            return check;
        }
        protected bool PostGenerateNetListConditions()
        {
            for(int i = 0; i < newList.inputPortList.Count; i++)
            {
                if (newList.inputPortList[i].node == 0) return false;
            }
            for (int i = 0; i < newList.outputPortList.Count; i++)
            {
                if (newList.outputPortList[i].node == 0) return false;
            }
            return true;
        }
        protected void DeleteComponent()
        {
            if (selectedType == RES) newList.resistorList.RemoveAt(selectedIndex);
            if (selectedType == LIN) newList.lineList.RemoveAt(selectedIndex);
            if (selectedType == IND) newList.inductorList.RemoveAt(selectedIndex);
            if (selectedType == CAP) newList.capacitorList.RemoveAt(selectedIndex);
            if (selectedType == GND) newList.groundList.RemoveAt(selectedIndex);
            if (selectedType == INP) newList.inputPortList.RemoveAt(selectedIndex);
            if (selectedType == OUT) newList.outputPortList.RemoveAt(selectedIndex);
            componentSelected = false;
            greyRectangle = Rectangle.Empty;
            toolStripLabel3.Enabled = false;
            selectedIndex = -1;
            selectedType = -1;
            for (int i = 0; i < newList.lineList.Count; i++)
                newList.lineList[i].ExecuteSolder(i);
            this.Refresh();
            toolStrip1.Refresh();
        }
        protected void ClearGreyComponents()
        {
            greyList.Clear();
            gridSnapRectangle = Rectangle.Empty;
        }
       
        protected int GetExistingNode(Point terminal)
        {
            for (int i = 0; i < newList.resistorList.Count; i++)
            {
                if (newList.resistorList[i].terminal1 == terminal && newList.resistorList[i].node1 != -1) return newList.resistorList[i].node1;
                else if (newList.resistorList[i].terminal2 == terminal && newList.resistorList[i].node2 != -1) return newList.resistorList[i].node2;
            }
            for (int i = 0; i < newList.capacitorList.Count; i++)
            {
                if (newList.capacitorList[i].terminal1 == terminal && newList.capacitorList[i].node1 != -1) return newList.capacitorList[i].node1;
                else if (newList.capacitorList[i].terminal2 == terminal && newList.capacitorList[i].node2 != -1) return newList.capacitorList[i].node2;
            }
            for (int i = 0; i < newList.inductorList.Count; i++)
            {
                if (newList.inductorList[i].terminal1 == terminal && newList.inductorList[i].node1 != -1) return newList.inductorList[i].node1;
                else if (newList.inductorList[i].terminal2 == terminal && newList.inductorList[i].node2 != -1) return newList.inductorList[i].node2;
            }
            for (int i = 0; i < newList.lineList.Count; i++)
            {
                if (newList.lineList[i].terminal1 == terminal && newList.lineList[i].node1 != -1) return newList.lineList[i].node1;
                else if (newList.lineList[i].terminal2 == terminal && newList.lineList[i].node2 != -1) return newList.lineList[i].node2;
            }
            for (int i = 0; i < newList.groundList.Count; i++)
            {
                if (newList.groundList[i].terminal == terminal && newList.groundList[i].node != -1) return newList.groundList[i].node;

            }
            return -1;
        }
        protected void DrawGreyComponent()
        {
            if (greyList.greyLine != null) greyList.greyLine.DrawLine(this.CreateGraphics(), Color.Gray);
            if (greyList.greyLine2 != null) greyList.greyLine2.DrawLine(this.CreateGraphics(), Color.Gray);
            if (greyList.greyResistor != null) greyList.greyResistor.DrawComponent(this.CreateGraphics(), Color.Gray);
            if (greyList.greyCapacitor != null) greyList.greyCapacitor.DrawComponent(this.CreateGraphics(), Color.Gray);
            if (greyList.greyInductor != null) greyList.greyInductor.DrawComponent(this.CreateGraphics(), Color.Gray);
            if (greyList.greyGround != null) greyList.greyGround.DrawComponent(this.CreateGraphics(), Color.Gray);
            if (greyList.greyInputPort != null) greyList.greyInputPort.DrawComponent(this.CreateGraphics(), INP, Color.Gray);
            if (greyList.greyOutputPort != null) greyList.greyOutputPort.DrawComponent(this.CreateGraphics(), OUT, Color.Gray);
            if (greyRectangle.IsEmpty == false) this.CreateGraphics().DrawRectangle(new Pen(Color.Gray), greyRectangle);
        }
        protected void DrawListItems()
        {
            int count;
            if (newList == null) return;
            for (count = 0; count < newList.lineList.Count; count++)
            {
                newList.lineList[count].DrawLine(this.CreateGraphics());
                //newList.lineList[count].DrawValues(this.CreateGraphics());
            }
            for (count = 0; count < newList.resistorList.Count; count++)
            {
                newList.resistorList[count].DrawComponent(this.CreateGraphics());
                //newList.resistorList[count].DrawDetails(this.CreateGraphics());
            }
            for (count = 0; count < newList.capacitorList.Count; count++)
            {
                newList.capacitorList[count].DrawComponent(this.CreateGraphics());
                //newList.capacitorList[count].DrawDetails(this.CreateGraphics());
            }
            for (count = 0; count < newList.inductorList.Count; count++)
            {
                newList.inductorList[count].DrawComponent(this.CreateGraphics());
                //newList.inductorList[count].DrawDetails(this.CreateGraphics());
            }
            for (count = 0; count < newList.groundList.Count; count++)
            {
                newList.groundList[count].DrawComponent(this.CreateGraphics());
                //newList.groundList[count].DrawDetails(this.CreateGraphics());
            }
            for (count = 0; count < newList.inputPortList.Count; count++)
            {
                newList.inputPortList[count].DrawComponent(this.CreateGraphics(), INP);
            }
            for (count = 0; count < newList.outputPortList.Count; count++)
            {
                newList.outputPortList[count].DrawComponent(this.CreateGraphics(), OUT);
            }
        }
        private void Draw(Graphics graphics)
        {
            if (gridSnapRectangle != null) DrawGridSnap();
            DrawListItems();
            DrawGreyComponent();
            if (component == 0) toolStripStatusLabel1.Text = "";
            if (component == 1) toolStripStatusLabel1.Text = "Draw wire";
            if (component == 2) toolStripStatusLabel1.Text = "Place resistor";
            if (component == 3) toolStripStatusLabel1.Text = "Place capacitor";
            if (component == 4) toolStripStatusLabel1.Text = "Place inductor";
            if (component == 5) toolStripStatusLabel1.Text = "Place ground terminal";
            if (component == 6) toolStripStatusLabel1.Text = "Select Input Port";
            if (component == 7) toolStripStatusLabel1.Text = "Select Output Port";
            statusStrip1.Refresh();
        }

        protected override void OnPaint(PaintEventArgs paintEventArgs)
        {
            base.OnPaint(paintEventArgs);
            //graphics.DrawLine(new Pen(Color.Black), new Point(20, 30), new Point(50, 70));
            Draw(this.CreateGraphics());
        }
       
        protected override void OnClick(EventArgs e)
        {

            bool callNow = false;
            base.OnClick(e);
            if (component == 0)
            {

                CheckClickableZone();
            }

            if (component == LIN)
            {
                if (continueClick == true)
                {
                    startDone = true;
                    point = endPoint;

                }
                if (startDone == false || firstClick == true)
                {
                    greyList.greyLine = null;
                    greyList.greyLine2 = null;
                    point = gridCoordinates(this.PointToClient(Control.MousePosition));
                    startDone = true;
                }
                else if (startDone == true)
                {

                    endPoint = gridCoordinates(this.PointToClient(Control.MousePosition));
                    if ((new Line(point, endPoint, false, -1)).ConnectedTerminal(endPoint) == true)
                    {
                        callNow = true;   
                    }
                    continueClick = true;
                    startDone = false;
                    Line line = new Line(point, endPoint, true, -1);
                    newList.lineList.Add(line);
                   if(callNow == true)
                    {
                        point = Point.Empty; endPoint = Point.Empty;
                        continueClick = false;
                        firstClick = false;
                        return;
                    }
                        point = endPoint;
                    
                    
                    ExecuteAllSolder();
                }
                firstClick = false;

                this.Refresh();
                toolStrip1.Refresh();
            }
            if (component == RES) newList.resistorList.Add(new Resistor(0, gridCoordinates(this.PointToClient(MousePosition)), false));
            if (component == CAP) newList.capacitorList.Add(new Capacitor(0, gridCoordinates(this.PointToClient(MousePosition)), false));
            if (component == IND) newList.inductorList.Add(new Inductor(0, gridCoordinates(this.PointToClient(MousePosition)), false));
            if (component == GND) newList.groundList.Add(new Ground(gridCoordinates(this.PointToClient(MousePosition))));
            if (component == INP) newList.inputPortList.Add(new Port(gridCoordinates(this.PointToClient(MousePosition)), INP));
            if (component == OUT) newList.outputPortList.Add(new Port(gridCoordinates(this.PointToClient(MousePosition)), INP));


        }
        protected override void OnDoubleClick(EventArgs e)
        {
            base.OnDoubleClick(e);
            CheckDoubleClickableZone();

        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (component != 0)
            {
                gridSnapRectangle = new Rectangle(gridCoordinates(this.PointToClient(Control.MousePosition)).X - 1,
                    gridCoordinates(this.PointToClient(Control.MousePosition)).Y - 1, 2, 2);

                switch (component)
                {
                    case 1:
                        if (firstClick == false)
                            greyList.greyLine = new Line(point, gridCoordinates(this.PointToClient(Control.MousePosition)), false,-1);
                        break;
                    case 2:
                        greyList.greyResistor = new Resistor(0, gridCoordinates(this.PointToClient(Control.MousePosition)), true);
                        break;
                    case 3:
                        greyList.greyCapacitor = new Capacitor(0, gridCoordinates(this.PointToClient(Control.MousePosition)), true);
                        break;
                    case 4:
                        greyList.greyInductor = new Inductor(0, gridCoordinates(this.PointToClient(Control.MousePosition)),true);
                        break;
                    case 5:

                        greyList.greyGround = new Ground(gridCoordinates(this.PointToClient(Control.MousePosition)));
                        break;
                    case 6:
                        greyList.greyInputPort = new Port(gridCoordinates(this.PointToClient(Control.MousePosition)), INP);
                        break;
                    case 7:
                        greyList.greyOutputPort = new Port(gridCoordinates(this.PointToClient(Control.MousePosition)), OUT);
                        break;
                }

            }
            if (component != 1)
            {
                greyList.greyLine = null;
                greyList.greyLine2 = null;
            }
            if (component != 2) greyList.greyResistor = null;
            if (component != 3) greyList.greyCapacitor = null;
            if (component != 4) greyList.greyInductor = null;
            if (component != 5) greyList.greyGround = null;
            if (component != 6) greyList.greyInputPort = null;
            if (component != 7) greyList.greyOutputPort = null;
            this.Refresh();
        }


        private void toolStripButton1_Click(object sender, EventArgs e)
        {

            if (component == 1)
            {
                component = 0;
                continueClick = false;
            }
            else if (component != 1)
            {
                component = 1;
                firstClick = true;
                continueClick = false;
            }
            this.Refresh();
        }
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (component == 2)
            {
                component = 0;
                continueClick = false;
            }
            else if (component != 2) component = 2;
            this.Refresh();
        }
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (component == 3)
            {
                component = 0;
                continueClick = false;
            }
            else if (component != 3) component = 3;
            this.Refresh();
        }
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (component == 4)
            {
                component = 0;
                continueClick = false;
            }
            else if (component != 4) component = 4;
            this.Refresh();
        }
        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ClearGreyComponents();
        }
        private bool GenerateNetList()
        {
            pendingList = new ComponentList(newList);
            for(int i = 0; i < pendingList.resistorList.Count; i++)
            {
                if (pendingList.resistorList[i].node1 == UNDEFINED || pendingList.resistorList[i].node2 == UNDEFINED
                    || pendingList.resistorList[i].ConnectedTerminal(pendingList.resistorList[i].terminal1) == false
                    || pendingList.resistorList[i].ConnectedTerminal(pendingList.resistorList[i].terminal2) == false)
                {
                    pendingList.resistorList.RemoveAt(i);
                    i--;
                }
                else if (pendingList.resistorList[i].value == 0)
                {
                    DialogResult = MessageBox.Show("Some components have zero value. Continue to generate netlist?", "Components with zero value", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (DialogResult == DialogResult.No) return false;
                }
            }
            for (int i = 0; i < pendingList.capacitorList.Count; i++)
            {
                if (pendingList.capacitorList[i].node1 == UNDEFINED || pendingList.capacitorList[i].node2 == UNDEFINED
                    || pendingList.capacitorList[i].ConnectedTerminal(pendingList.capacitorList[i].terminal1) == false
                    || pendingList.capacitorList[i].ConnectedTerminal(pendingList.capacitorList[i].terminal2) == false)
                {
                    pendingList.capacitorList.RemoveAt(i);
                    i--;
                }
                else if (pendingList.capacitorList[i].value == 0)
                {
                    DialogResult = MessageBox.Show("Some components have zero value. Continue to generate netlist?", "Components with zero value", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (DialogResult == DialogResult.No) return false;
                }
            }
            for (int i = 0; i < pendingList.inductorList.Count; i++)
            {
                if (pendingList.inductorList[i].node1 == UNDEFINED || pendingList.inductorList[i].node2 == UNDEFINED
                    || pendingList.inductorList[i].ConnectedTerminal(pendingList.inductorList[i].terminal1) == false
                    || pendingList.inductorList[i].ConnectedTerminal(pendingList.inductorList[i].terminal2) == false)
                {
                    pendingList.inductorList.RemoveAt(i);
                    i--;
                }
                else if (pendingList.inductorList[i].value == 0)
                {
                    DialogResult = MessageBox.Show("Some components have zero value. Continue to generate netlist?", "Components with zero value", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (DialogResult == DialogResult.No) return false;
                }
            }
            List<string> netList = new List<string>();
            string title = "SIMULATION - S_PARAMETERS";
            for(int i = 0; i < pendingList.resistorList.Count; i++)
            {
                string node1String, node2String;
                if (pendingList.resistorList[i].node1 == pendingList.inputPortList[0].node) node1String = "in";
                else if (pendingList.resistorList[i].node1 == pendingList.outputPortList[0].node) node1String = "out";
                else node1String = pendingList.resistorList[i].node1.ToString();
                if (pendingList.resistorList[i].node2 == pendingList.inputPortList[0].node) node2String = "in";
                else if (pendingList.resistorList[i].node2 == pendingList.outputPortList[0].node) node2String = "out";
                else node2String = pendingList.resistorList[i].node2.ToString();
                netList.Add("R" + (i + 1).ToString() + " " + node1String +" " + node2String + " " +
                    pendingList.resistorList[i].value.ToString());
            }
            for (int i = 0; i < pendingList.inductorList.Count; i++)
            {
                string node1String, node2String;
                if (pendingList.inductorList[i].node1 == pendingList.inputPortList[0].node) node1String = "in";
                else if (pendingList.inductorList[i].node1 == pendingList.outputPortList[0].node) node1String = "out";
                else node1String = pendingList.inductorList[i].node1.ToString();
                if (pendingList.inductorList[i].node2 == pendingList.inputPortList[0].node) node2String = "in";
                else if (pendingList.inductorList[i].node2 == pendingList.outputPortList[0].node) node2String = "out";
                else node2String = pendingList.inductorList[i].node2.ToString();
                netList.Add("L" + (i + 1).ToString() + " " + node1String + " " + node2String + " " +
                    pendingList.inductorList[i].value.ToString());
            }
            for (int i = 0; i < pendingList.capacitorList.Count; i++)
            {
                string node1String, node2String;
                if (pendingList.capacitorList[i].node1 == pendingList.inputPortList[0].node) node1String = "in";
                else if (pendingList.capacitorList[i].node1 == pendingList.outputPortList[0].node) node1String = "out";
                else node1String = pendingList.capacitorList[i].node1.ToString();
                if (pendingList.capacitorList[i].node2 == pendingList.inputPortList[0].node) node2String = "in";
                else if (pendingList.capacitorList[i].node2 == pendingList.outputPortList[0].node) node2String = "out";
                else node2String = pendingList.capacitorList[i].node2.ToString();
                netList.Add("C" + (i + 1).ToString() + " " + node1String + " " + node2String + " " +
                    pendingList.capacitorList[i].value.ToString());
            }
            StreamWriter tempNetListFileHandle = new StreamWriter("newNetList.cir");
            tempNetListFileHandle.WriteLine(title);
            for (int i = 0; i < netList.Count; i++)
            {
                tempNetListFileHandle.WriteLine(netList[i]);
            }
            try
            {
                StreamReader partFile =  new StreamReader("firstpart.cir");
                while (partFile.EndOfStream == false)
                {
                    string bufferString;
                    bufferString = partFile.ReadLine();
                    tempNetListFileHandle.WriteLine(bufferString);
                }
                partFile.Close();
            }
            catch(Exception e)
            {
                MessageBox.Show("Raw files (firstpart.cir, secondpart.cir, thirdpart.cir) for s-parameter analysis not found!","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            string incString = null;
            string numString = null;
            if (sParameterData.incrementType == LINEAR)
            {
                incString = "lin";
                numString = sParameterData.totalPoints.ToString();
            }
            if (sParameterData.incrementType == DECADE)
            {
                incString = "dec";
                numString = Math.Floor((sParameterData.totalPoints * (Math.Log10(sParameterData.endFrequency) -
                    Math.Log10(sParameterData.startFrequency)))).ToString();
            }
            if (sParameterData.incrementType == OCTAVE)
            {
                incString = "oct";
                numString = (sParameterData.totalPoints * Math.Floor((Math.Log(sParameterData.endFrequency, 2) -
                    Math.Log(sParameterData.startFrequency, 2)))).ToString();
            }
            string ac = "ac " + incString + " " + numString + " " + sParameterData.startFrequency + " " + sParameterData.endFrequency;
            tempNetListFileHandle.WriteLine(ac);
            try
            {
                StreamReader partFile = new StreamReader("secondpart.cir");
                while (partFile.EndOfStream == false)
                {
                    string bufferString;
                    bufferString = partFile.ReadLine();
                    tempNetListFileHandle.WriteLine(bufferString);
                }
                partFile.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Raw files for s-parameter analysis not found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            tempNetListFileHandle.WriteLine(ac);
            try
            {
                StreamReader partFile = new StreamReader("thirdpart.cir");
                while (partFile.EndOfStream == false)
                {
                    string bufferString;
                    bufferString = partFile.ReadLine();
                    tempNetListFileHandle.WriteLine(bufferString);
                }
                partFile.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Raw files for s-parameter analysis not found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            tempNetListFileHandle.Close();
            string sourceInputString = "source " + "newNetList.cir";
            //ProcessStartInfo processInfo = new ProcessStartInfo("ngshared_test.exe", sourceInputString);
            

            
            return true;
            
        }
        private void UpdateNodes(Point updateTerminal, int node)
        {
            for (int i = 0; i < newList.groundList.Count; i++)
            {
                if (newList.groundList[i].terminal == updateTerminal) newList.groundList[i].node = node;
                
            }
            for (int i = 0; i < newList.inputPortList.Count; i++)
            {
                if (newList.inputPortList[i].terminal == updateTerminal) newList.inputPortList[i].node = node;
            }
            for (int i = 0; i < newList.outputPortList.Count; i++)
            {
                if (newList.outputPortList[i].terminal == updateTerminal) newList.outputPortList[i].node = node;
            }
            for (int i = 0; i < newList.resistorList.Count; i++)
            {
                if (newList.resistorList[i].terminal1 == updateTerminal) newList.resistorList[i].node1 = node;
                if (newList.resistorList[i].terminal2 == updateTerminal) newList.resistorList[i].node2 = node;
            }
            for (int i = 0; i < newList.inductorList.Count; i++)
            {
                if (newList.inductorList[i].terminal1 == updateTerminal) newList.inductorList[i].node1 = node;
                if (newList.inductorList[i].terminal2 == updateTerminal) newList.inductorList[i].node2 = node;
            }
            for (int i = 0; i < newList.capacitorList.Count; i++)
            {
                if (newList.capacitorList[i].terminal1 == updateTerminal) newList.capacitorList[i].node1 = node;
                if (newList.capacitorList[i].terminal2 == updateTerminal) newList.capacitorList[i].node2 = node;
            }
            for(int i =0; i < newList.outputPortList.Count; i++)
            {
                if (newList.outputPortList[i].terminal == updateTerminal) newList.outputPortList[i].node = node;
            }
            for(int i = 0; i < newList.lineList.Count; i++)
            {
                if (newList.lineList[i].terminal1 == updateTerminal) newList.lineList[i].node1 = node;
                if (newList.lineList[i].terminal2 == updateTerminal) newList.lineList[i].node2 = node;
                newList.lineList[i].ExecuteSolder(i);
            }
            
        }
        private void DefinePartialNodes() 
        {
            con:
            for (int i = 0; i < newList.lineList.Count; i++)
            {
                if (newList.lineList[i].PartialNodeDefined() == true)
                {
                    if (newList.lineList[i].node1 == UNDEFINED)
                    {
                        newList.lineList[i].node1 = newList.lineList[i].node2;
                        UpdateNodes(newList.lineList[i].terminal1, newList.lineList[i].node1);
                    }

                    else if (newList.lineList[i].node2 == UNDEFINED)
                    {
                        newList.lineList[i].node2 = newList.lineList[i].node1;
                        UpdateNodes(newList.lineList[i].terminal2, newList.lineList[i].node2);
                    }
                    goto con;

                }
            }
            for (int i =0; i < newList.resistorList.Count; i++)
            {

                if(newList.resistorList[i].PartialNodeDefined() == true)
                {
                    if(newList.resistorList[i].node1 == UNDEFINED)
                    {
                        newList.resistorList[i].node1 = ++nodeIndex;
                        UpdateNodes(newList.resistorList[i].terminal1, newList.resistorList[i].node1);
                    }
                        
                    else if(newList.resistorList[i].node2 == UNDEFINED)
                    {
                        newList.resistorList[i].node2 = ++nodeIndex;
                        UpdateNodes(newList.resistorList[i].terminal2, newList.resistorList[i].node2);
                    }
                        
                        goto con; // perform Partial Node Definition on modified list
                }
            }
            for (int i = 0; i < newList.capacitorList.Count; i++)
            {
                if (newList.capacitorList[i].PartialNodeDefined() == true)
                {
                    if (newList.capacitorList[i].node1 == UNDEFINED)
                    {
                        newList.capacitorList[i].node1 = ++nodeIndex;
                        UpdateNodes(newList.capacitorList[i].terminal1, newList.capacitorList[i].node1);
                    }
                        
                    if (newList.capacitorList[i].node2 == UNDEFINED)
                    {
                        newList.capacitorList[i].node2 = ++nodeIndex;
                        UpdateNodes(newList.capacitorList[i].terminal2, newList.capacitorList[i].node2);
                    }
                        

                    
                    goto con; // perform Partial Node Definition on modified list
                }
            
            }
            for (int i = 0; i < newList.inductorList.Count; i++)
            {
                if (newList.inductorList[i].PartialNodeDefined() == true)
                {
                    if (newList.inductorList[i].node1 == UNDEFINED)
                    {
                        newList.inductorList[i].node1 = ++nodeIndex;
                        UpdateNodes(newList.inductorList[i].terminal1, newList.inductorList[i].node1);
                    }
                        
                    if (newList.inductorList[i].node2 == UNDEFINED)
                    {
                        newList.inductorList[i].node2 = ++nodeIndex;
                        UpdateNodes(newList.inductorList[i].terminal2, newList.inductorList[i].node2);
                    }

                    goto con; // perform Partial Node Definition on modified list
                    
                }
            }
            
        }
        /*private void DetectAndSetWireNodes(object obj)
        {
            /*if (obj.GetType() == typeof(ElectricalComponent))
            {
                Point startPoint1 = ((ElectricalComponent)obj).terminal1;
                Point startPoint2 = ((ElectricalComponent)obj).terminal2;
                for(int i = 0; i < newList.lineList.Count; i++)
                {
                    if(newList.lineList[i].check == false)
                    {
                        if (startPoint1 == newList.lineList[i].terminal1 || startPoint1 == newList.lineList[i].terminal2)
                        {
                            newList.lineList[i].node1 = newList.lineList[i].node2 = ((ElectricalComponent)obj).node1;
                        }
                        if (startPoint2 == newList.lineList[i].terminal1 || startPoint2 == newList.lineList[i].terminal2)
                        {
                            newList.lineList[i].node1 = newList.lineList[i].node2 = ((ElectricalComponent)obj).node2;
                        }
                    }
                    
                }
            }
            else if (obj.GetType() == typeof(Ground)) { }
            if (obj.GetType() == typeof(Port))
            {
                Point portPoint = ((Port)obj).terminal;
                for (int i = 0; i < newList.lineList.Count; i++)
                {   
                        if (portPoint == newList.lineList[i].terminal1 || portPoint == newList.lineList[i].terminal2)
                        {
                            newList.lineList[i].node1 = newList.lineList[i].node2 = ((Port)obj).node;
                            newList.lineList[i].check = true;
                            UpdateNodes(portPoint, ((Port)obj).node);

                        }
                        if (portPoint == newList.lineList[i].terminal1) DetectAndSetWireNodes(newList.lineList[i].terminal2);
                        if (portPoint == newList.lineList[i].terminal2) DetectAndSetWireNodes(newList.lineList[i].terminal1);
                }
            }
        }*/
        private void SetNode(int oldValue, int newValue)
        {
            for(int i = 0; i < newList.resistorList.Count; i++)
            {
                if (newList.resistorList[i].node1 == oldValue) newList.resistorList[i].node1 = newValue;
                if (newList.resistorList[i].node2 == oldValue) newList.resistorList[i].node2 = newValue;
            }
            for (int i = 0; i < newList.inductorList.Count; i++)
            {
                if (newList.inductorList[i].node1 == oldValue) newList.inductorList[i].node1 = newValue;
                if (newList.inductorList[i].node2 == oldValue) newList.inductorList[i].node2 = newValue;
            }
            for (int i = 0; i < newList.capacitorList.Count; i++)
            {
                if (newList.capacitorList[i].node1 == oldValue) newList.capacitorList[i].node1 = newValue;
                if (newList.capacitorList[i].node2 == oldValue) newList.capacitorList[i].node2 = newValue;
            }
            for (int i = 0; i < newList.lineList.Count; i++)
            {
                if (newList.lineList[i].node1 == oldValue) newList.lineList[i].node1 = newValue;
                if (newList.lineList[i].node2 == oldValue) newList.lineList[i].node2 = newValue;
            }
            for (int i = 0; i < newList.inputPortList.Count; i++)
            {
                if (newList.inputPortList[i].node == oldValue) newList.inputPortList[i].node = newValue;
                
            }
            for (int i = 0; i < newList.outputPortList.Count; i++)
            {
                if (newList.outputPortList[i].node == oldValue) newList.outputPortList[i].node = newValue;
                
            }
            for (int i = 0; i < newList.groundList.Count; i++)
            {
                if (newList.groundList[i].node == oldValue) newList.groundList[i].node = newValue;

            }
        }
        private void GenerateNodes()
        {
            nodeIndex = 1;
            pendingList = newList;
            for(int i = 0; i < newList.resistorList.Count; i++)
            {
                newList.resistorList[i].node1 = UNDEFINED;
                newList.resistorList[i].node2 = UNDEFINED;

            }
            for (int i = 0; i < newList.capacitorList.Count; i++)
            {
                newList.capacitorList[i].node1 = UNDEFINED;
                newList.capacitorList[i].node2 = UNDEFINED;
            }
            for (int i = 0; i < newList.inductorList.Count; i++)
            {
                newList.inductorList[i].node1 = UNDEFINED;
                newList.inductorList[i].node2 = UNDEFINED;
            }
            for (int i = 0; i < newList.groundList.Count; i++)
            {
                newList.groundList[i].node = UNDEFINED;
                
            }
            for (int i = 0; i < newList.inputPortList.Count; i++)
            {
                newList.inputPortList[i].node = UNDEFINED;

            }
            for (int i = 0; i < newList.outputPortList.Count; i++)
            {
                newList.outputPortList[i].node = UNDEFINED;

            }
            for (int i = 0; i < newList.lineList.Count; i++)
            {
                newList.lineList[i].node1 = UNDEFINED;
                newList.lineList[i].node2 = UNDEFINED;
                newList.lineList[i].solder1 = false;
                newList.lineList[i].solder2 = false;
                newList.lineList[i].ExecuteSolder(i);
            }
            newList.inputPortList[0].node = 1;
            newList.inputPortList[0].check = true;// input port to be set to node 1
            pendingList = newList;
            UpdateNodes(newList.inputPortList[0].terminal, newList.inputPortList[0].node);
            DefinePartialNodes();
            for (int i = 0; i < Program.newForm.newList.groundList.Count; i++)
            {
                if (newList.groundList[i].node == UNDEFINED)
                {
                    MessageBox.Show("All Grounds are not connected to circuit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else SetNode(newList.groundList[i].node, 0);
                
            }

            bool successNetList = false;
            // only one ground in circuit
            bool safePorts = PostGenerateNetListConditions();
            if(safePorts == true)
                successNetList = GenerateNetList();
            if (successNetList == true) MessageBox.Show("Netlist created", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information); // elements with nodes
            statusStrip1.Refresh();
            Refresh();
        }
        private void netlistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool[] check = CheckGenerateNetlistConditions();
            if (check[0] == false)
            {
                MessageBox.Show("Only one input / output port is allowed", "Improper number of ports", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (check[1] == false)
            {
                MessageBox.Show("Port terminals are not allowed to be connected to ground", "Improper port terminal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            GenerateNodes();

        }
        
        private void Form1_KeyPress(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                component = 0;
                continueClick = false;
                gridSnapRectangle = Rectangle.Empty;
                greyList = new GreyList();
                this.Refresh();
            }
            if(e.KeyCode == Keys.Delete)
            {
                if (componentSelected == true)
                    DeleteComponent();
            }
        }
        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }
        private void incrementToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void toolStripLabel3_Click(object sender, EventArgs e)
        {
            if (componentSelected == true)
            {
                DeleteComponent();
            }
        }
        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if (component == 5)
            {
                component = 0;
                continueClick = false;
            }
            else if (component != 5) component = 5;
            this.Refresh();
        }
        private void frequencyRangeToolStripMenuItem_Click(object sender, EventArgs e)
        {



        }

        private void toolStripLabel4_Click(object sender, EventArgs e)
        {
            if (alignment == true)
            {
                alignment = false;
                toolStripLabel4.Text = "VERTICAL";
            }
            else
            {
                alignment = true;
                toolStripLabel4.Text = "HORIZONTAL";
            }
            toolStrip1.Refresh();
        }

        private void netlistFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 netListForm = new Form2("newNetList.cir");
            netListForm.Show();
        }

        private void s2pFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 netListForm = new Form2(currentS2PFile);
            netListForm.Show();
        }

        private void s2pFilesimulateToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ProcessStartInfo processInfo = new ProcessStartInfo();
            processInfo.CreateNoWindow = false;
            processInfo.FileName = "ngshared_test.exe";
           
            processInfo.Arguments = "newNetList.cir"; // send .cir files as arguments without .cir extension. Example "netlist" for netlist.cir
            try
            {
                currentS2PFile = "newNetList.s2p";
                Process process = Process.Start(processInfo);
                process.WaitForExit();
                
            }
            catch(Exception exc)
            {
                if(exc.GetType() == typeof(FileNotFoundException))
                {
                    currentS2PFile = "newNetList.s2p";
                    toolStripStatusLabel1.Text = "Please Wait...";
                    toolStrip1.Refresh();
                    //MessageBox.Show("NGSPICE executable not found. Using builtin code", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ProgramSpice.MainSpice(new string[] { processInfo.Arguments });
                    toolStripStatusLabel1.Text = "";
                    return;
                }
                if (exc.GetType() == typeof(Win32Exception))
                {
                    currentS2PFile = "newNetList.s2p";
                    toolStripStatusLabel1.Text = "Please Wait...";
                    toolStrip1.Refresh();
                    //MessageBox.Show("NGSPICE executable not found. Using builtin code", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ProgramSpice.MainSpice(new string[] { processInfo.Arguments });
                    toolStripStatusLabel1.Text = "";
                    return;
                }
               
                else
                {
                    MessageBox.Show(exc.GetType().ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }
            
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.openFileDialog1 = new OpenFileDialog();
            openFileDialog1.ShowDialog();
            string filename = openFileDialog1.FileName;
            if (filename.Split('.').Length < 2) return;
            if (filename.Split('.')[1] != "raw")
            {
                MessageBox.Show("Not a valid .raw file!", "Invalid file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            ProcessStartInfo processInfo = new ProcessStartInfo();
            processInfo.CreateNoWindow = false;
            processInfo.FileName = "ngshared_test.exe";
            processInfo.Arguments = filename;
            try
            {
                currentS2PFile = openFileDialog1.FileName.Split('.')[0] + ".s2p";
                Process process = Process.Start(processInfo);
                process.WaitForExit();
                currentS2PFile = openFileDialog1.FileName.Split('.')[0] + ".s2p";
            }
            catch (Exception exc)
            {
                if (exc.GetType() == typeof(FileNotFoundException))
                {
                    //MessageBox.Show("Using builtin code", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ProgramSpice.MainSpice(new string[] { filename });
                    return;
                }
                if (exc.GetType() == typeof(Win32Exception))
                {
                    //MessageBox.Show("Using builtin code", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ProgramSpice.MainSpice(new string[] { filename });

                    return;
                }
                
                else
                {
                    //ProgramSpice.MainSpice(new string[] { filename });
                    MessageBox.Show(exc.GetType().ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }

        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            
        }
        public void ExecuteAllSolder()
        {
            for (int i = 0; i < newList.lineList.Count; i++)
                newList.lineList[i].ExecuteSolder(i);
        }
        public string ManagedUnit(string unmanagedNumberString)
        {
            string postfix = null;
            double unmanagedNumber;
            int thousandPower;
            string managedNumberString;
            bool success = double.TryParse(unmanagedNumberString, out unmanagedNumber);
            if (success == true && unmanagedNumber > 0)
            {
                double log = Math.Log10(unmanagedNumber);
                thousandPower = (int)(log / 3);
            }
            else return unmanagedNumberString;
            switch (thousandPower)
            {
                case 1:
                    postfix = "K"; break;
                case 2:
                    postfix = "Meg"; break;
                case 3:
                    postfix = "G"; break;
                case 4:
                    postfix = "T"; break;
                case -1:
                    postfix = "m"; break;
                case -2:
                    postfix = "u"; break;
                case -3:
                    postfix = "n"; break;
                case -4:
                    postfix = "p"; break;
                case -5:
                    postfix = "f"; break;
                default: break;
            }
            managedNumberString = (unmanagedNumber / Math.Pow(10, 3 * thousandPower)).ToString() + postfix;
            return managedNumberString;
        }
        private string UnmanagedUnit(string managedNumberString)
        {
            int indexOfPostfix = -1;
            double unmanagedNumber;
            int power = 0;
            for(int i = 0; i < managedNumberString.Length; i++)
            {
                if (char.IsLetter(managedNumberString[i]))
                {
                    indexOfPostfix = i;
                    break;
                }
            }
            if (indexOfPostfix > -1)
            {
                string postfix = managedNumberString.Substring(indexOfPostfix);
                string unmanagedNumberString = managedNumberString.Substring(0, indexOfPostfix);
                switch (postfix.ToLower())
                {
                    case "k": power = 3; break;
                    case "meg": power = 6; break;
                    case "g": power = 9; break;
                    case "t": power = 12; break;
                    case "m": power = -3; break;
                    case "u": power = -6; break;
                    case "n": power = -9; break;
                    case "p": power = -12; break;
                    case "f": power = -15; break;
                }
                bool success = double.TryParse(unmanagedNumberString, out unmanagedNumber);
                if (success == true)
                {
                    unmanagedNumber *= Math.Pow(10, power);
                    return unmanagedNumber.ToString();
                }
                else return null;
            }
            else return managedNumberString;
        }

       

        private void viewHelpFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 helpForm = new Form2(1);
            helpForm.Show();
                        
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 helpForm = new Form2(2);
            helpForm.Show();

        }

        

        private void toolStripLabel5_Click(object sender, EventArgs e)
        {
            if (componentSelected == true)
            {
                UpdateToolStripTextBox1TextShowDetails(true);
                UpdateToolStripTextBox1TextShowDetails(false);
            }
            toolStrip1.Refresh();
            this.Refresh();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void setStartFrequencyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double editValue;
            string inputStart = Interaction.InputBox("Enter the starting frequency:", "Edit Frequency Range", ManagedUnit(sParameterData.startFrequency.ToString()));
            inputStart = UnmanagedUnit(inputStart);
            bool goodValue = double.TryParse(inputStart, out editValue);
            if (goodValue == false && inputStart != "" || editValue < 0 && goodValue == true)
            {
                MessageBox.Show("Invalid frequency! Refer to help for input rules", "Attention!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                setStartFrequencyToolStripMenuItem_Click(sender, e);
                return;
            }
            if (goodValue == true) sParameterData.startFrequency = editValue;
        }
        private void linearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sParameterData.incrementType = LINEAR;
            decadeToolStripMenuItem.Checked = false;
            octaveToolStripMenuItem.Checked = false;

        }
        private void decadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sParameterData.incrementType = DECADE;
            linearToolStripMenuItem.Checked = false;
            octaveToolStripMenuItem.Checked = false;
        }
        private void octaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sParameterData.incrementType = OCTAVE;
            linearToolStripMenuItem.Checked = false;
            decadeToolStripMenuItem.Checked = false;
        }
        private void setTotalNumberOfPointsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double editValue;
            string inputTotal = Interaction.InputBox("Enter total number of points:", "Edit TotalNumber of Points", ManagedUnit(sParameterData.totalPoints.ToString()));
            inputTotal = UnmanagedUnit(inputTotal);
            bool goodValue = double.TryParse(inputTotal, out editValue);
            if (goodValue == false && inputTotal != "" || editValue < 0 && goodValue == true)
            {
                MessageBox.Show("Invalid number! Refer to help for input rules", "Attention!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                setStartFrequencyToolStripMenuItem_Click(sender, e);
                return;
            }
            if (goodValue == true) sParameterData.totalPoints = editValue;
        }
        private void setEndFrequencyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double editValue;
            string inputEnd = Interaction.InputBox("Enter the ending frequency:", "Edit Frequency Range", ManagedUnit(sParameterData.endFrequency.ToString()));
            inputEnd = UnmanagedUnit(inputEnd);
            bool goodValue = double.TryParse(inputEnd, out editValue);
            if (goodValue == false && inputEnd != "" || editValue < 0 && goodValue == true)
            {
                MessageBox.Show("Invalid frequency!", "Attention!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                setStartFrequencyToolStripMenuItem_Click(sender, e);
                return;
            }
            if (goodValue == true) sParameterData.endFrequency = editValue;
        }
        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            if (component == 6)
            {
                component = 0;
                continueClick = false;
            }
            else if (component != 6) component = 6;
            this.Refresh();
        }
        private void toolStripLabel2_Click(object sender, EventArgs e)
        {
            if (component == 7)
            {
                component = 0;
                continueClick = false;
            }
            else if (component != 7) component = 7;
            this.Refresh();
        }
        private void menuStrip1_ItemClicked(object sender, EventArgs e) { }
        private void statusStrip1_ItemClicked(object sender, EventArgs e) { }
        private void generateToolStripMenuItem_Click(object sender, EventArgs e) { }
        private void Form1_Load(object sender, EventArgs e)
        {
           
        }
        
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Clear the circuit schematic diagram?", "Attention!", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            if (result == DialogResult.Yes)
            {
                newList = new ComponentList();
            }
            this.Refresh();
        }


    }




    // Class Definitions

    public class Line
    {
        public Rectangle imageRectangle;
        public Point terminal1, terminal2;
        public int node1, node2;
        public static int width = 4;
        public bool solder1, solder2;

        public bool check;
        public void DrawValues(Graphics graphics)
        {
            int font = 5;
            Point pointCoordinates1 = new Point(terminal1.X, terminal1.Y);
            Point pointCoordinates2 = new Point(terminal2.X, terminal2.Y);
            Point pointNode1 = new Point(pointCoordinates1.X, pointCoordinates1.Y + font + 1);
            Point pointNode2 = new Point(pointCoordinates2.X, pointCoordinates2.Y + font + 1);
            graphics.DrawString(terminal1.ToString(), new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointCoordinates1);
            graphics.DrawString(terminal2.ToString(), new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointCoordinates2);
            graphics.DrawString(node1.ToString(), new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointNode1);
            graphics.DrawString(node2.ToString(), new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointNode2);
        }
        public bool PartialNodeDefined()
        {
            if (node1 != Form1.UNDEFINED && node2 == Form1.UNDEFINED) return true;
            else if (node1 == Form1.UNDEFINED && node2 != Form1.UNDEFINED) return true;
            else return false;
        }
        public Line(Point x, Point y, bool createArgumentPermission, int exceptionIndex)
        {
            check = false;
            solder1 = solder2 = false;
            node1 = node2 = Form1.UNDEFINED;
            if (Math.Abs(x.X - y.X) > Math.Abs(x.Y - y.Y)) Program.newForm.toggleShape = false;
            else Program.newForm.toggleShape = true;
            if (x.X != y.X && x.Y != y.Y)
            {
                if (Program.newForm.toggleShape == true)
                {
                    terminal1 = new Point(x.X, x.Y);
                    terminal2 = new Point(x.X, y.Y);

                    Line extraLine = new Line(terminal2, y, false, -1);

                    if (createArgumentPermission == true)
                    {
                        extraLine.ExecuteSolder();
                        Program.newForm.newList.lineList.Add(extraLine);
                        
                    }
                }
                else if (Program.newForm.toggleShape == false)
                {
                    terminal1 = new Point(x.X, x.Y);
                    terminal2 = new Point(y.X, x.Y);
                    Line extraLine = new Line(terminal2, y, false, -1);
                    if (createArgumentPermission == true)
                    {
                        extraLine.ExecuteSolder();
                        Program.newForm.newList.lineList.Add(extraLine);
                    }
                }
            }

            else
            {
                terminal1 = x;
                terminal2 = y;

            }
                imageRectangle = new Rectangle(Math.Min(terminal1.X, terminal2.X) - 2, Math.Min(terminal1.Y, terminal2.Y) - 2, Math.Abs(terminal1.X - terminal2.X) + 4, Math.Abs(terminal1.Y - terminal2.Y) + 4);
                ExecuteSolder(exceptionIndex);
            
        }
        public void ExecuteSolder() { ExecuteSolder(-1); }
        public void ExecuteSolder(int exceptionIndex)
            {

                for (int i = 0; i < Program.newForm.newList.lineList.Count; i++)
                    if (this.Equals(Program.newForm.newList.lineList[i]) == false)
                    {
                        if (exceptionIndex == i) continue;

                        if (Program.newForm.newList.lineList[i].terminal1.X == this.terminal1.X &&
                            this.terminal1.X == Program.newForm.newList.lineList[i].terminal2.X &&
                            (Program.newForm.newList.lineList[i].terminal1.Y < this.terminal1.Y &&
                            this.terminal1.Y < Program.newForm.newList.lineList[i].terminal2.Y ||
                            Program.newForm.newList.lineList[i].terminal1.Y > this.terminal1.Y &&
                            this.terminal1.Y > Program.newForm.newList.lineList[i].terminal2.Y))
                        {
                            node1 = Program.newForm.newList.lineList[i].node1;
                            solder1 = true;
                            break;
                        }
                        else solder1 = false;
                        if (Program.newForm.newList.lineList[i].terminal1.X == this.terminal2.X &&
                           this.terminal2.X == Program.newForm.newList.lineList[i].terminal2.X &&
                           (Program.newForm.newList.lineList[i].terminal1.Y < this.terminal2.Y &&
                           this.terminal2.Y < Program.newForm.newList.lineList[i].terminal2.Y ||
                           Program.newForm.newList.lineList[i].terminal1.Y > this.terminal2.Y &&
                           this.terminal2.Y > Program.newForm.newList.lineList[i].terminal2.Y))
                        {
                            node2 = Program.newForm.newList.lineList[i].node1;
                            solder2 = true;
                            break;
                        }
                        else solder2 = false;
                        if (Program.newForm.newList.lineList[i].terminal1.Y == this.terminal1.Y &&
                          this.terminal1.Y == Program.newForm.newList.lineList[i].terminal2.Y &&
                          (Program.newForm.newList.lineList[i].terminal1.X < this.terminal1.X &&
                          this.terminal1.X < Program.newForm.newList.lineList[i].terminal2.X ||
                          Program.newForm.newList.lineList[i].terminal1.X > this.terminal1.X &&
                          this.terminal1.X > Program.newForm.newList.lineList[i].terminal2.X))
                        {
                            node1 = Program.newForm.newList.lineList[i].node1;
                            solder1 = true;
                            break;
                        }
                        else solder1 = false;
                        if (Program.newForm.newList.lineList[i].terminal1.Y == this.terminal2.Y &&
                             this.terminal2.Y == Program.newForm.newList.lineList[i].terminal2.Y &&
                             (Program.newForm.newList.lineList[i].terminal1.X < this.terminal2.X &&
                             this.terminal2.X < Program.newForm.newList.lineList[i].terminal2.X ||
                             Program.newForm.newList.lineList[i].terminal1.X > this.terminal2.X &&
                             this.terminal2.X > Program.newForm.newList.lineList[i].terminal2.X))
                        {
                            node2 = Program.newForm.newList.lineList[i].node2;
                            solder2 = true;
                            break;
                        }
                        else solder2 = false;
                    }
            }
        public void StaticDrawTerminal(Graphics graphics, Point center, Point center2)
        {
            if (ConnectedTerminal(center) == false && solder1 == false)
            {
                graphics.DrawEllipse(new Pen(Color.Blue), center.X - width / 2, center.Y - width / 2, width, width);
            }
            if (solder1 == true) graphics.FillEllipse(new SolidBrush(Color.Blue), center.X - width / 2, center.Y - width / 2, width, width);
            if (ConnectedTerminal(center2) == false && solder2 == false)
            {
                graphics.DrawEllipse(new Pen(Color.Blue), center2.X - width / 2, center2.Y - width / 2, width, width);
            }
            if (solder2 == true) graphics.FillEllipse(new SolidBrush(Color.Blue), center2.X - width / 2, center2.Y - width / 2, width, width);
        }
        public bool ConnectedTerminal(Point argPoint)
        {
            for (int i = 0; i < Program.newForm.newList.lineList.Count; i++)
            {
                    if (Program.newForm.newList.lineList[i] == this) continue;
                    else if (Program.newForm.newList.lineList[i].terminal1 == argPoint || Program.newForm.newList.lineList[i].terminal2 == argPoint)
                    {
                        return true;
                    }
                }
                for (int i = 0; i < Program.newForm.newList.resistorList.Count; i++)
                {
                    if (Program.newForm.newList.resistorList[i].terminal1 == argPoint || Program.newForm.newList.resistorList[i].terminal2 == argPoint)
                        return true;
                }
                for (int i = 0; i < Program.newForm.newList.capacitorList.Count; i++)
                {
                    if (Program.newForm.newList.capacitorList[i].terminal1 == argPoint || Program.newForm.newList.capacitorList[i].terminal2 == argPoint)
                        return true;
                }
                for (int i = 0; i < Program.newForm.newList.inductorList.Count; i++)
                {
                    if (Program.newForm.newList.inductorList[i].terminal1 == argPoint || Program.newForm.newList.inductorList[i].terminal2 == argPoint)
                        return true;
                }
                for (int i = 0; i < Program.newForm.newList.groundList.Count; i++)
                {
                    if (Program.newForm.newList.groundList[i].terminal == argPoint)
                        return true;
                }
                for (int i = 0; i < Program.newForm.newList.inputPortList.Count; i++)
                {
                    if (Program.newForm.newList.inputPortList[i].terminal == argPoint)
                        return true;
                }
                for (int i = 0; i < Program.newForm.newList.outputPortList.Count; i++)
                {
                    if (Program.newForm.newList.outputPortList[i].terminal == argPoint)
                        return true;
                }
                return false;
            }
            public void DrawLine(Graphics graphics)
            {
                DrawLine(graphics, Color.Black);
            }
            public void DrawLine(Graphics graphics, Color color)
            {

                if (color == Color.Gray && terminal1.X != terminal2.X && terminal1.Y != terminal2.Y)
                {
                    if (Math.Abs(terminal1.X - terminal2.X) > Math.Abs(terminal1.Y - terminal2.Y)) Program.newForm.toggleShape = true;
                    else Program.newForm.toggleShape = false;

                    Line extraLine1 = new Line(new Point(0, 0), new Point(0, 0), false, -1);
                    if (Program.newForm.toggleShape == true)
                    {
                        extraLine1.terminal1 = terminal1;
                        extraLine1.terminal2 = new Point(terminal1.X, terminal2.Y);
                    }
                    else if (Program.newForm.toggleShape == false)
                    {
                        extraLine1.terminal1 = terminal1;
                        extraLine1.terminal2 = new Point(terminal2.X, terminal1.Y);
                    }
                    Line extraLine2 = new Line(extraLine1.terminal2, terminal2, false, -1);
                    Program.newForm.greyList.greyLine2 = extraLine2;
                    graphics.DrawLine(new Pen(Color.Red), terminal1, extraLine1.terminal2);
                    graphics.DrawLine(new Pen(Color.Red), extraLine2.terminal1, terminal2);
                }
                else graphics.DrawLine(new Pen(color), terminal1, terminal2);
                StaticDrawTerminal(graphics, terminal1, terminal2);
            }

        }
    
    public class ElectricalComponent
    {
        public double value;
        public bool showDetails;
        public bool horizontalAlignment;
        public Rectangle imageRectangle;
        public Point terminal1, terminal2;
        public int node1, node2;
        public static int width = 4, lineDimension = 4;
        public static int LIN = 1, RES = 2, CAP = 3, IND = 4, GND = 5;
        public bool check;
        public Point center;
        public int imageDimension = Form1.componentDimension, componentHeight = Form1.componentDimension * 2 / 3;
        public void StaticDrawTerminal(Graphics graphics, Point argPoint, Point argPoint2)
        {
            if(ConnectedTerminal(argPoint) == false)
                graphics.DrawEllipse(new Pen(Color.Blue, 1), argPoint.X - width / 2, argPoint.Y - width / 2, width, width);
            else
                graphics.DrawEllipse(new Pen(Color.Blue,1), argPoint.X-1, argPoint.Y-1, width / 2, width / 2);
            if(ConnectedTerminal(argPoint2) == false)
                graphics.DrawEllipse(new Pen(Color.Blue,1), argPoint2.X - width / 2, argPoint2.Y - width / 2, width, width);
            else
                graphics.DrawEllipse(new Pen(Color.Blue, 1), argPoint2.X-1, argPoint2.Y-1, width / 2, width / 2);
        }
        public bool ConnectedTerminal(Point argPoint)
        {
            for (int i = 0; i < Program.newForm.newList.lineList.Count; i++)
            {
                if (Program.newForm.newList.lineList[i].terminal1 == argPoint || Program.newForm.newList.lineList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.resistorList.Count; i++)
            {
                if (Program.newForm.newList.resistorList[i] == this) continue;
                if (Program.newForm.newList.resistorList[i].terminal1 == argPoint || Program.newForm.newList.resistorList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.capacitorList.Count; i++)
            {
                if (Program.newForm.newList.capacitorList[i] == this) continue;
                if (Program.newForm.newList.capacitorList[i].terminal1 == argPoint || Program.newForm.newList.capacitorList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.inductorList.Count; i++)
            {
                if (Program.newForm.newList.inductorList[i] == this) continue;
                if (Program.newForm.newList.inductorList[i].terminal1 == argPoint || Program.newForm.newList.inductorList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.groundList.Count; i++)
            {
                if (Program.newForm.newList.groundList[i].terminal == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.inputPortList.Count; i++)
            {
                if (Program.newForm.newList.inputPortList[i].terminal == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.outputPortList.Count; i++)
            {
                if (Program.newForm.newList.outputPortList[i].terminal == argPoint)
                    return true;
            }
            return false;
        }
        public void DrawDetails(Graphics graphics)
        {
            string detailsString = null;
            int font = 5;
            if (this.GetType() == typeof(Resistor)) detailsString = "Res";
            if (this.GetType() == typeof(Capacitor)) detailsString = "Cap";
            if (this.GetType() == typeof(Inductor)) detailsString = "Ind";
            Point pointNode = new Point(center.X, center.Y + imageDimension / 4);
            Point pointType = new Point(center.X, pointNode.Y + font + 1);
            Point pointTerm = new Point(center.X, pointType.Y + font + 1);
            Point pointTerm2 = new Point(center.X, pointTerm.Y + font + 1);
            Point pointValue = new Point(center.X, pointTerm2.Y + font + 1);
            graphics.DrawString("(" + node1.ToString()+", "+node2.ToString() + ")", new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointNode);
            graphics.DrawString(Program.newForm.ManagedUnit(value.ToString()), new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointType);
            //graphics.DrawString(terminal1.ToString(), new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointTerm);
            //graphics.DrawString(terminal2.ToString(), new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointTerm2);

        }
        
        /*public void DrawTerminal(Graphics graphics, int type, Point point)
        {
            graphics.DrawEllipse(new Pen(Color.Blue), terminal1.X - width / 2, terminal1.Y - width / 2, width, width);
            graphics.DrawEllipse(new Pen(Color.Blue), terminal2.X - width / 2, terminal2.Y - width / 2, width, width);
        }*/
        public bool PartialNodeDefined()
        {
            if (node1 != Form1.UNDEFINED && node2 == Form1.UNDEFINED) return true;
            else if (node1 == Form1.UNDEFINED && node2 != Form1.UNDEFINED) return true;
            else return false;
        }


        /*public ElectricalComponent(double value, int type, Point point, Point point2)
        {

            check = false;
            node1 = node2 = -1;
            horizontalAlignment = Program.newForm.alignment;
            terminal1 = point;
            terminal2 = point2;
            imageRectangle = new Rectangle(terminal1.X, terminal1.Y - imageDimension / 2, imageDimension, imageDimension);
            
        }*/

        public ElectricalComponent(Point center, bool greyStatus)
        {
            if (Program.newForm.alignment == true)
            {
                horizontalAlignment = true;
                terminal1.X = center.X - imageDimension / 2;
                terminal1.Y = terminal2.Y = center.Y;
                terminal2.X = center.X + imageDimension / 2;
                imageRectangle = new Rectangle(terminal1.X, terminal1.Y - imageDimension / 2, imageDimension, imageDimension);
            }
            else if (Program.newForm.alignment == false)
            {
                horizontalAlignment = false;
                terminal1.Y = center.Y - imageDimension / 2;
                terminal1.X = terminal2.X = center.X;
                terminal2.Y = center.Y + imageDimension / 2;
                imageRectangle = new Rectangle(terminal1.X - imageDimension / 2, terminal1.Y, imageDimension, imageDimension);
            }
            horizontalAlignment = Program.newForm.alignment;
            check = false;
            showDetails = true;
            node1 = node2 = -1;
            this.center = center;
            componentHeight = Form1.componentDimension / 3;
            if(greyStatus == false) InsertOnWire();

        }
        public void InsertOnWire()
        {
            for(int i = 0; i < Program.newForm.newList.lineList.Count; i++)
            {
                if(Program.newForm.newList.lineList[i].terminal1.X == Program.newForm.newList.lineList[i].terminal2.X)
                {
                    if(terminal1.X == terminal2.X && terminal2.X == Program.newForm.newList.lineList[i].terminal1.X &&
                        Program.newForm.newList.lineList[i].terminal1.Y < terminal1.Y && 
                        terminal1.Y < Program.newForm.newList.lineList[i].terminal2.Y &&
                        Program.newForm.newList.lineList[i].terminal1.Y < terminal2.Y &&
                        terminal2.Y < Program.newForm.newList.lineList[i].terminal2.Y)
                    {
                        Line extraLine = new Line(terminal1, Program.newForm.newList.lineList[i].terminal1, false, i);
                        Program.newForm.newList.lineList[i] = new Line(Program.newForm.newList.lineList[i].terminal2, terminal2, false, i);
                        Program.newForm.newList.lineList.Add(extraLine);
                        Program.newForm.Refresh();
                    }
                    if (terminal1.X == terminal2.X && terminal2.X == Program.newForm.newList.lineList[i].terminal1.X &&
                        Program.newForm.newList.lineList[i].terminal1.Y > terminal1.Y &&
                        terminal1.Y > Program.newForm.newList.lineList[i].terminal2.Y &&
                        Program.newForm.newList.lineList[i].terminal1.Y > terminal2.Y &&
                        terminal2.Y > Program.newForm.newList.lineList[i].terminal2.Y)
                    {

                        Line extraLine = new Line(terminal1, Program.newForm.newList.lineList[i].terminal2, false, i);
                        Program.newForm.newList.lineList[i] = new Line(Program.newForm.newList.lineList[i].terminal1, terminal2, false, i);
                        Program.newForm.newList.lineList.Add(extraLine);
                        Program.newForm.Refresh();
                    }
                    
                }
                if (Program.newForm.newList.lineList[i].terminal1.Y == Program.newForm.newList.lineList[i].terminal2.Y)
                {
                    if (terminal1.Y == terminal2.Y && terminal2.Y == Program.newForm.newList.lineList[i].terminal1.Y &&
                        Program.newForm.newList.lineList[i].terminal1.X < terminal1.X &&
                        terminal1.X < Program.newForm.newList.lineList[i].terminal2.X &&
                        Program.newForm.newList.lineList[i].terminal1.X < terminal2.X &&
                        terminal2.X < Program.newForm.newList.lineList[i].terminal2.X )
                    {
                        Line extraLine = new Line(terminal2, Program.newForm.newList.lineList[i].terminal2, false, i);
                        Program.newForm.newList.lineList[i] = new Line(Program.newForm.newList.lineList[i].terminal1, terminal1, false, i);
                        Program.newForm.newList.lineList.Add(extraLine);
                        Program.newForm.Refresh();
                    }
                    if (terminal1.Y == terminal2.Y && terminal2.Y == Program.newForm.newList.lineList[i].terminal1.Y &&
                        Program.newForm.newList.lineList[i].terminal1.X > terminal1.X &&
                        terminal1.X > Program.newForm.newList.lineList[i].terminal2.X &&
                        Program.newForm.newList.lineList[i].terminal1.X > terminal2.X &&
                        terminal2.X > Program.newForm.newList.lineList[i].terminal2.X)
                    {
                        Line extraLine = new Line(terminal2, Program.newForm.newList.lineList[i].terminal1, false, i);
                        Program.newForm.newList.lineList[i] = new Line(Program.newForm.newList.lineList[i].terminal2, terminal1, false, i);
                        Program.newForm.newList.lineList.Add(extraLine);
                        Program.newForm.Refresh();
                    }

                }
            }
            
        }
    }
    public class Resistor : ElectricalComponent
    {

        
        int wireLen = 8;
        public Resistor(double argValue, Point argCenter, bool greyStatus) : base(argCenter, greyStatus)
        {
            value = argValue;
            componentHeight = Form1.componentDimension / 4;
        }

        public void DrawComponent(Graphics graphics) { DrawComponent(graphics, Color.Black); }
        public void DrawComponent(Graphics graphics, Color color) { DrawComponent(graphics, center, color); }
        public void DrawComponent(Graphics graphics, Point point, Color color)
        {
            //graphics.DrawString("abcd", new Font(FontFamily.GenericMonospace,7), new SolidBrush(Color.Black), point);
            /*Point[] drawPoints = new Point[]
                        {
                        new Point(point.X - imageDimension / 2, point.Y),
                        new Point(point.X - 5 * imageDimension / 14, point.Y),
                        new Point(point.X - 3 * imageDimension / 14, point.Y - componentHeight),
                        new Point(point.X - 1 * imageDimension / 14, point.Y + componentHeight),
                        new Point(point.X + imageDimension / 12, point.Y - componentHeight),
                        new Point(point.X + 3 * imageDimension / 14, point.Y + componentHeight),
                        new Point(point.X + 5 * imageDimension / 14, point.Y),
                        new Point(point.X + imageDimension / 2, point.Y),

                        };*/

            Point[] drawPoint = new Point[]
            {
                            new Point(point.X - imageDimension / 2, point.Y),
                            new Point(point.X - imageDimension / 2 + wireLen, point.Y)
                            
            };
            Point[] drawPoint2 = new Point[]
            {
                new Point(point.X + imageDimension / 2, point.Y),
                            new Point(point.X + imageDimension / 2 - wireLen, point.Y)
            };
            Point[] drawPointRotated = new Point[]
            {
                            new Point(point.X, point.Y - imageDimension / 2),
                            new Point(point.X, point.Y - imageDimension / 2 + wireLen)
            };
            Point[] drawPoint2Rotated = new Point[]
            {
                new Point(point.X, point.Y + imageDimension / 2),
                            new Point(point.X, point.Y + imageDimension / 2 - wireLen)
            };
            Rectangle drawRectangle = new Rectangle(point.X - imageDimension / 2 + wireLen, point.Y - componentHeight / 2, imageDimension - 2 * wireLen, componentHeight);
            Rectangle drawRectangle2 = new Rectangle(point.X - componentHeight / 2, point.Y - imageDimension / 2 + wireLen, componentHeight, imageDimension - 2 * wireLen);
            if (horizontalAlignment == true)
            {
                graphics.DrawRectangle(new Pen(color), drawRectangle);
                graphics.DrawLines(new Pen(color), drawPoint);
                graphics.DrawLines(new Pen(color), drawPoint2);
            }
            else
            {
                graphics.DrawRectangle(new Pen(color), drawRectangle2);
                graphics.DrawLines(new Pen(color), drawPointRotated);
                graphics.DrawLines(new Pen(color), drawPoint2Rotated);
            }

                //graphics.DrawLines(new Pen(color), drawPoints);
                StaticDrawTerminal(graphics, terminal1, terminal2);
            if (color == Color.Black)
            {
                if(showDetails == true) DrawDetails(graphics);
            }
        }
    }
    public class Capacitor : ElectricalComponent
    {
        public Capacitor(double argValue, Point argCenter, bool greyStatus) : base(argCenter, greyStatus)
        {
            value = argValue;
           
            
        }
        public void DrawComponent(Graphics graphics) { DrawComponent(graphics, Color.Black); }
        public void DrawComponent(Graphics graphics, Color color) { DrawComponent(graphics, center, color); }
        public void DrawComponent(Graphics graphics, Point point, Color color)
        {
            Point[] drawPoints = new Point[]
                {
                        new Point(point.X - imageDimension / 2, point.Y),
                        new Point(point.X - 2, point.Y),
                        new Point(point.X - 2, point.Y - componentHeight),
                        new Point(point.X - 2, point.Y + componentHeight)
                };
            Point[] drawPointsRotated = new Point[]
                {
                        new Point(point.X, point.Y - imageDimension / 2),
                        new Point(point.X, point.Y - 2),
                        new Point(point.X - componentHeight, point.Y - 2),
                        new Point(point.X + componentHeight, point.Y -2)
                };
            Point[] drawPoints1 = new Point[]
            {
                        new Point(point.X + imageDimension / 2, point.Y),
                        new Point(point.X + 2, point.Y),
                        new Point(point.X + 2, point.Y - componentHeight),
                        new Point(point.X + 2, point.Y + componentHeight)
            };
            Point[] drawPoints1Rotated = new Point[]
            {
                        new Point(point.X, point.Y + imageDimension / 2),
                        new Point(point.X, point.Y + 2),
                        new Point(point.X - componentHeight, point.Y + 2),
                        new Point(point.X + componentHeight, point.Y + 2)
            };
            if (horizontalAlignment == true)
            {
                graphics.DrawLines(new Pen(color), drawPoints);
                graphics.DrawLines(new Pen(color), drawPoints1);
            }
            else
            {
                graphics.DrawLines(new Pen(color), drawPointsRotated);
                graphics.DrawLines(new Pen(color), drawPoints1Rotated);
            }
            StaticDrawTerminal(graphics, terminal1, terminal2);
            if (color == Color.Black)
            {
                if(showDetails == true) DrawDetails(graphics);
            }
        }
    }
    public class Inductor : ElectricalComponent
    {
        int wireLen = 6;
        
        public Inductor(double argValue, Point argCenter, bool greyStatus) : base(argCenter, greyStatus)
        {
            value = argValue;
            componentHeight = Form1.componentDimension * 1 / 5;
          
        }
        public void DrawComponent(Graphics graphics) { DrawComponent(graphics, Color.Black); }
        public void DrawComponent(Graphics graphics, Color color) { DrawComponent(graphics, center, color); }
        public void DrawComponent(Graphics graphics, Point point, Color color)
        {
            Point[] drawPoints = new Point[]
            {
                        new Point(point.X - imageDimension / 2, point.Y),
                        new Point(point.X - imageDimension / 2 + wireLen, point.Y)
            };
            Point[] drawPointsRotated = new Point[]
            {
                        new Point(point.X, point.Y - imageDimension / 2),
                        new Point(point.X, point.Y -imageDimension / 2 + wireLen)
            };
            Point[] drawPoints1 = new Point[]
            {
                        new Point(point.X + imageDimension / 2, point.Y),
                        new Point(point.X + imageDimension / 2 - wireLen, point.Y)
            };
            Point[] drawPoints1Rotated = new Point[]
            {
                        new Point(point.X, point.Y + imageDimension / 2),
                        new Point(point.X, point.Y + imageDimension / 2 - wireLen)
            };
            int numberOfArcs = 3;
            int arcWidth = (imageDimension - 2 * wireLen) / numberOfArcs;
            if(horizontalAlignment == true)
            {
                graphics.DrawLines(new Pen(color), drawPoints);
                graphics.DrawLines(new Pen(color), drawPoints1);
                graphics.DrawArc(new Pen(color), point.X - imageDimension / 2 + wireLen, point.Y - componentHeight, arcWidth, componentHeight * 2, 180, 180);
                graphics.DrawArc(new Pen(color), point.X - imageDimension / 2 + arcWidth + wireLen, point.Y - componentHeight, arcWidth, componentHeight * 2, 180, 180);
                graphics.DrawArc(new Pen(color), point.X - imageDimension / 2 + 2 * arcWidth + wireLen, point.Y - componentHeight, arcWidth, componentHeight * 2, 180, 180);
            }
            else
            {
                graphics.DrawLines(new Pen(color), drawPointsRotated);
                graphics.DrawLines(new Pen(color), drawPoints1Rotated);
                graphics.DrawArc(new Pen(color), point.X - componentHeight, point.Y - imageDimension / 2 + wireLen, componentHeight * 2, arcWidth, 270, 180);
                graphics.DrawArc(new Pen(color), point.X - componentHeight, point.Y - imageDimension / 2 + wireLen + arcWidth, componentHeight * 2, arcWidth, 270, 180);
                graphics.DrawArc(new Pen(color), point.X - componentHeight, point.Y - imageDimension / 2 + wireLen + 2 * arcWidth, componentHeight * 2, arcWidth, 270, 180);

            }
            StaticDrawTerminal(graphics, terminal1, terminal2);
            if (color == Color.Black)
            {
                if (showDetails == true) DrawDetails(graphics);
            }
        }
    }
    public class Ground
    {
        public Rectangle imageRectangle;
        public Point terminal, center;
        public static int width = 4;
        public int node;

        public int componentHeight = Form1.componentDimension / 3;
        public static int imageDimension = Form1.componentDimension;
        public bool ConnectedTerminal(Point argPoint)
        {
            for (int i = 0; i < Program.newForm.newList.lineList.Count; i++)
            {
                if (Program.newForm.newList.lineList[i].terminal1 == argPoint || Program.newForm.newList.lineList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.resistorList.Count; i++)
            {
                if (Program.newForm.newList.resistorList[i].terminal1 == argPoint || Program.newForm.newList.resistorList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.capacitorList.Count; i++)
            {
                if (Program.newForm.newList.capacitorList[i].terminal1 == argPoint || Program.newForm.newList.capacitorList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.inductorList.Count; i++)
            {
                if (Program.newForm.newList.inductorList[i].terminal1 == argPoint || Program.newForm.newList.inductorList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.groundList.Count; i++)
            {
                if (Program.newForm.newList.groundList[i] == this) continue;
                if (Program.newForm.newList.groundList[i].terminal == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.inputPortList.Count; i++)
            {
                if (Program.newForm.newList.inputPortList[i].terminal == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.outputPortList.Count; i++)
            {
                if (Program.newForm.newList.outputPortList[i].terminal == argPoint)
                    return true;
            }
            return false;
        }
        public Ground(Point argCenter)
        {
            center = argCenter;
            terminal.X = argCenter.X;
            terminal.Y = argCenter.Y- imageDimension / 2;
            node = Form1.UNDEFINED;
            imageRectangle = new Rectangle(center.X - imageDimension / 2, center.Y - imageDimension / 2, imageDimension, imageDimension);
        }
        public void DrawComponent(Graphics graphics) { DrawComponent(graphics, Color.Black); }

        public void DrawComponent(Graphics graphics, Color color)
        {
            Point[] drawPoints = new Point[]
            {
                    new Point(terminal.X, terminal.Y),
                    new Point(terminal.X, terminal.Y + imageDimension / 3),
                    new Point(terminal.X - 5, terminal.Y + imageDimension / 3),
                    new Point(terminal.X + 5, terminal.Y + imageDimension / 3)
            };
            Point[] drawpoints1 = new Point[]
            {
                    new Point(terminal.X - 3, terminal.Y + imageDimension / 3 + 2),
                    new Point(terminal.X + 3, terminal.Y + imageDimension / 3 + 2)
            };
            graphics.DrawLines(new Pen(color), drawPoints);
            graphics.DrawLines(new Pen(color), drawpoints1);
            StaticDrawTerminal(graphics, terminal);
            if (color == Color.Black)
            {
                DrawDetails(graphics);
            }
        }
        public void DrawDetails(Graphics graphics)
        {
            int font = 5;
            string detailsString = "Gnd";
            Point pointNode = new Point(center.X, center.Y + imageDimension / 4);
            Point pointType = new Point(center.X, pointNode.Y + font + 1);
            Point pointTerm = new Point(center.X, pointType.Y + font + 1);
            graphics.DrawString(node.ToString(), new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointNode);
            graphics.DrawString(detailsString, new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointType);
            //graphics.DrawString(center.ToString(), new Font(FontFamily.GenericSansSerif, font), new SolidBrush(Color.Blue), pointTerm);
        }
        public void StaticDrawTerminal(Graphics graphics, Point center)
        {
            if (ConnectedTerminal(center) == false)
                graphics.DrawEllipse(new Pen(Color.Blue), center.X - width / 2, center.Y - width / 2, width, width);
        }
    }
    public class Port
    {
        public static int INP = Form1.INP, OUT = Form1.OUT;
        public bool check;
        public int type;
        public Point terminal;
        public int node;
        public Rectangle imageRectangle;
        public static int width = 4;
        public Port(Point terminal, int type)
        {
            check = false;
            node = Form1.UNDEFINED;
            this.terminal = terminal;
            this.type = type;
            imageRectangle = new Rectangle(terminal.X - width, terminal.Y - width, 2 * width, 2 * width);
        }
        public bool ConnectedTerminal(Point argPoint)
        {
            for (int i = 0; i < Program.newForm.newList.lineList.Count; i++)
            {
                if (Program.newForm.newList.lineList[i].terminal1 == argPoint || Program.newForm.newList.lineList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.resistorList.Count; i++)
            {
                if (Program.newForm.newList.resistorList[i].terminal1 == argPoint || Program.newForm.newList.resistorList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.capacitorList.Count; i++)
            {
                if (Program.newForm.newList.capacitorList[i].terminal1 == argPoint || Program.newForm.newList.capacitorList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.inductorList.Count; i++)
            {
                if (Program.newForm.newList.inductorList[i].terminal1 == argPoint || Program.newForm.newList.inductorList[i].terminal2 == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.groundList.Count; i++)
            {
               
                if (Program.newForm.newList.groundList[i].terminal == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.inputPortList.Count; i++)
            {
                if (Program.newForm.newList.inputPortList[i] == this) continue;
                if (Program.newForm.newList.inputPortList[i].terminal == argPoint)
                    return true;
            }
            for (int i = 0; i < Program.newForm.newList.outputPortList.Count; i++)
            {
                if (Program.newForm.newList.outputPortList[i] == this) continue;
                if (Program.newForm.newList.outputPortList[i].terminal == argPoint)
                    return true;
            }
            return false;
        }
        public void DrawComponent(Graphics graphics, int type, Color color) { DrawComponent(graphics, type, terminal, color); }
        public void DrawComponent(Graphics graphics, int type) { DrawComponent(graphics, type, terminal, Color.Black); }
        public void DrawComponent(Graphics graphics, int type, Point point, Color color)
        {
            Rectangle drawRectangle = new Rectangle(point.X - width, point.Y - width, 2 * width, 2 * width);
            graphics.DrawRectangle(new Pen(new SolidBrush(Color.Red), 3), drawRectangle);
            StaticDrawTerminal(graphics, terminal);
            if (color == Color.Black)
            {
                DrawDetails(graphics, type, terminal);
            }
        }
        public void StaticDrawTerminal(Graphics graphics, Point center)
        {
            if(ConnectedTerminal(center) == false)
                graphics.DrawEllipse(new Pen(Color.Blue), center.X - width / 2, center.Y - width / 2, width, width);

        }
        public void DrawDetails(Graphics graphics, int type, Point point)
        {
            string detailsString = null;
            int font = 5;
            if (type == INP) detailsString = "INP";
            else if (type == OUT) detailsString = "OUT";
            Point pointType = new Point(point.X - 8 / 2, point.Y + 8 / 2);
            graphics.DrawString(detailsString, new Font(FontFamily.GenericSansSerif, font, FontStyle.Bold), new SolidBrush(Color.Blue), pointType);
            Point pointTerm = new Point(point.X - 8 / 2, point.Y + 8 / 2 + font + 1);
            //graphics.DrawString(terminal.ToString(), new Font(FontFamily.GenericSansSerif, font, FontStyle.Bold), new SolidBrush(Color.Blue), pointTerm);
        }
    }
    public class SParameterData
    {

        public double startFrequency, endFrequency;
        public int incrementType;
        public double totalPoints;
        public SParameterData(int argIncrementType, double start, double end, double total)
        {
            incrementType = argIncrementType;
            startFrequency = start;
            endFrequency = end;
            totalPoints = total;
        }
        public SParameterData()
        {
            incrementType = Form1.LINEAR;
            startFrequency = Math.Pow(10, 9);
            endFrequency = Math.Pow(10, 10);
            totalPoints = 1000;
        }

    }

    public class ComponentList
    {
        public List<Line> lineList = new List<Line>();
        public List<Resistor> resistorList = new List<Resistor>();
        public List<Capacitor> capacitorList = new List<Capacitor>();
        public List<Inductor> inductorList = new List<Inductor>();
        public List<Ground> groundList = new List<Ground>();
        public List<Port> inputPortList = new List<Port>();
        public List<Port> outputPortList = new List<Port>();
        public ComponentList() { }
        public ComponentList(ComponentList obj)
        {
            lineList = obj.lineList.ToList<Line>() ;
            resistorList = obj.resistorList.ToList<Resistor>();
            capacitorList = obj.capacitorList.ToList<Capacitor>();
            inductorList = obj.inductorList.ToList<Inductor>();
            groundList = obj.groundList.ToList<Ground>();
            inputPortList = obj.inputPortList.ToList<Port>();
            outputPortList = obj.outputPortList.ToList<Port>();
        }
        
    }
    public class GreyList
    {
        public Line greyLine = null;
        public Line greyLine2 = null;
        public Resistor greyResistor = null;
        public Capacitor greyCapacitor = null;
        public Inductor greyInductor = null;
        public Ground greyGround = null;
        public Port greyInputPort = null;
        public Port greyOutputPort = null;
        public GreyList() { }
        public void Clear()
        {
            greyLine = null;
            greyLine2 = null;
            greyResistor = null;
            greyCapacitor = null;
            greyInductor = null;
            greyGround = null;
            greyInputPort = null;
            greyOutputPort = null;
        }
    }
}